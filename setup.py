import sys

# for the fortran source files
import numpy
from numpy.distutils.core import Extension, setup

# for the cython code
from distutils import extension
from Cython.Build import cythonize

from magfield import __version__ as version


extensions = [
    extension.Extension("_pyvald", sources=['magfield/_pyvald.pyx'],
                        include_dirs=[numpy.get_include()]),
    extension.Extension("_pys3div", sources=['magfield/_pys3div.pyx'],
                        include_dirs=[numpy.get_include()]),
    Extension(name='_s3div', sources=['magfield/s3div.f90'])]

short_descr = "Magfield Package"

description = """
              This python module contains procedures for working with
              data that is common when dealing with spectra in four Stokes
              parameters.
              """

classifiers = [['Development Status :: 5 - Production/Stable',
                'Development Status :: 4 - Beta'][int('b' in version)],
               'Programming Language :: Python',
               'Intended Audience :: Science/Research',
               'Topic :: Scientific/Engineering :: Astronomy',
               'License :: OSI Approved :: GPLv3',
               'Operating System :: POSIX :: Linux']

setup(name="magfield",
      version=version,
      description=short_descr,
      author='N. Rusomarov',
      author_email='naum.rusomarov@gmail.com',
      long_description=description,
      platforms=['Linux'],
      license='GPLv3',
      classifiers=classifiers,
      ext_package="magfield",
      ext_modules=cythonize(extensions),
      package_dir={"magfield": "magfield"},
      scripts=['scripts/zupdate.py', 'scripts/zextract.py', 'scripts/zconcat.py'],
      packages=['magfield'])
