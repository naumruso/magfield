Installation Instructions
=========================

Requirements:
  - python 2.7
  - numpy
  - scipy
  - astropy

Optional packages:
  - matplotlib
  - pylineid (https://github.com/naumruso/pylineid)

There are three installation options:

1. To install the package, run the setup.py script in this
   directory as follows:

      `python setup.py config_fc --fcompiler=gnu95 install`

   This will install the package into your Python system.

2. If you prefer not to modify your Python installation,
   create a directory under which to install the package
   e.g., mydir. Then install as follows:

      `python setup.py config_fc --fcompiler=gnu95 install --install-lib mydir`

   To use the package you then need to include `mydir` in
   your PYTHONPATH.

3. You can also issue the following command

      `python setup.py config_fc --fcompiler=gnu95 install --user`

   to install the package in your local python directory.

                                            03-09-2015, NR

## About the Synmast code. 

  The modules vstrat.py and pysynth.py depend on the
  executable `synmast` to be available through the `$PATH`
  system variable. This code is not freely available and
  must be requested from O. Kochukhov (oleg.kochukhov@physics.uu.se.).
  
  I don't own, support, nor have any other right to Synmast.
  
                                            05-07-2018, NR
