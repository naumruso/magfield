from __future__ import print_function, absolute_import, division

import numpy as np
from scipy.io import FortranFile

from ._s3div import disk_integration
from ._pys3div import read_mout

def _read_mout(lines, nst=1):
    """
    Deprecated because dog slow.

    Pure Python function that reads and parses text file output from Synmast.
    It works but it's slow and has been superceded by a similar Cython function.

    Change nst to read the other Stokes parameters if they are present
    in the calculations.
    """
    # read the number of spectral lines
    line = lines.pop(0)
    nlin = int(line.split('-')[0])

    # remove the first nlin lines
    for i in range(nlin):
        lines.pop(0)

    # number of angles
    line = lines.pop(0)
    nmu = int(line.strip())

    # read the angles
    line = lines.pop(0)
    mu = np.float64(line.split())

    # read the number of wavelength points for the continuum intensity
    # and create the arrays
    line = lines.pop(0)
    nwc = int(line.strip())
    wlc = np.zeros(nwc, dtype=np.float64)
    spc = np.zeros((nwc,nmu), dtype=np.float64, order='F')

    for i in range(nwc):
        line = lines.pop(0).split()
        wlc[i] = np.float64(line[0])
        spc[i,:] = np.float64(line[1:])

    # read the number of wavelength points in the line spectrum
    line = lines.pop(0)
    nwl = int(line.strip())
    wll = np.zeros(nwl, dtype=np.float64)
    spl = np.zeros((nst,nwl,nmu), dtype=np.float64, order='F')
    for ist in range(nst):
        for iwl in range(nwl):
            line = lines.pop(0).split()
            wll[iwl] = np.float64(line[0])
            spl[ist,iwl,:] = np.float64(line[1:])
    return mu, wlc, spc, wll, spl

def read_bin(ff):
    """
    Reads binary files outputted by Synmast_bin. It plays a similar role
    to _read_mout and read_mout except that it's much faster than both
    of them because it doesn't need to parse any ascii data.

    The input variable ff is a scipy.io.FortranFile object. 

    The data have been outputted with the following Fortran statements:
    #  WRITE(FOUT) NMU
    #  WRITE(FOUT) (XMU(IMU),IMU=1,NMU)
    #
    #  WRITE(FOUT) NWLCNT
    #  WRITE(FOUT) WFIRST
    #  WRITE(FOUT) WLCNT(1:NWLCNT)
    #  WRITE(FOUT) CNT(1:NMU,1:NWLCNT)
    #
    #  WRITE(FOUT) M
    #  WRITE(FOUT) NWL
    #  WRITE(FOUT) WL(1:NWL)
    #  DO I=1, M
    #  WRITE(FOUT) STOKES(1:NMU,I,1:NWL)
    #  ENDDO

    Note: Each Fortran write statement writes a single record that can be
    read as an equivalent numpy array by scipy. Other use is error prone.
    """
    nmu = ff.read_ints()[0]
    xmu = ff.read_record(dtype=np.float32)
    
    nwlcnt = ff.read_ints()[0]
    wfirst = ff.read_reals()
    wlcnt = ff.read_record(dtype=np.float32) + wfirst
    cnt = ff.read_record(dtype=np.float64).reshape(nwlcnt,nmu)

    nst = ff.read_ints()[0]
    nwl = ff.read_ints()[0]
    wl = ff.read_record(dtype=np.float64) + wfirst
    
    #Now the actual Stokes parameters
    stokes = []
    for i in range(nst):
        stokes.append(ff.read_record(dtype=np.float64).reshape(nwl,nmu))
    
    res = {}
    res['NMU'] = nmu
    res['XMU'] = xmu
    res['NWLCNT'] = nwlcnt
    res['WLCNT'] = wlcnt
    res['CNT'] = cnt
    res['NST'] = nst
    res['NWL'] = nwl
    res['WL'] = wl
    res['STOKES'] = stokes
    return res

def s3div_spectrum(mu,wlc,spc,wll,spl,vsini=0.0,vrt=0.0,resol=1e6,rinst=0.0):
    """
    A simple wrapper function for the subroutine s3div from the Fortran
    module disk_integration. It expects input that can be acquired from
    the functions read_bin and read_mout.

    Input arguments:
      - mu: sine of the angles
      - wlc: wavelengths for the continuum intensity
      - spc: continuum intensity array
      - wll: wavelengths for the absorption line spectrum
      - spl: the line absorption spectrum
      - vsini: projected rotational velocity (km/s)
      - vrt: radial-tangential velocity (km/s)
      - resol: resolution of the data outputted by Synmast (default=1e6)
      - rinst: desired resolution of the integrated output spectrum

    # the input arrays have shapes:
    # nmu, mu(nmu) # integration angles
    # nwc, wlc(nwc), spc(nwc,nmu) # continuum
    # nwl, wll(nwl), spl(nwl,nmu) # line spectrum
    """
    # the width of the convolution window in points and the resolution
    nx = int( np.log( wlc[-1] / wlc[0] ) / np.log( 1. + 1. / resol ) + 1)
    resol = 1.0 / ( ( wlc[-1] / wlc[0] ) ** ( 1.0 / ( nx - 1.0 ) ) - 1.0 )

    wl, flxl, flxc, ierror = disk_integration.s3div(mu, wlc, spc, wll, spl, 
                                                    vsini, vrt, resol, rinst, nx)
    return wl, flxl/flxc

def s3div_ascii(filename, vsini=0.0, vrt=0.0, resol=1e6, rinst=0.0, nst=1):
    """
    Works with ascii files that are outputted by the regular Synmast code.

    The function by default integrates only the intensity spectra. Change the
    'nst' parameter to integrate the rest of the Stokes parameters if they are present.

    Other parameters are:
      - vsini, projected rotational velocity in km/s, 
      - vrt, radial-tangential velocity in km/s, 
      - resolution, resolution of the output data read from 'filename'
      - rinst, desired instrumental resolution
    """
    #parse the input file
    with open(filename) as fin:
        mu, wlc, spc, wll, spl = read_mout(fin.readlines(), nst=nst)

    #integrate the Stokes parameters one by one
    wl = []
    sp = []
    for spl_ in spl:
        wl_, sp_ = s3div_spectrum(mu,wlc,spc,wll,spl_,vsini,vrt,resol,rinst)
        wl.append(wl_)
        sp.append(sp_)
    return wl, sp

def s3div_bin(filename, vsini=0.0, vrt=0.0, resol=1e6, rinst=0.0, nst=1):
    """
    Works with binary Fortran files that are outputted by the modified Synmast code
    that saves the output data as a Fortran records instead of an ascii file that
    needs to be parsed line by line.

    The function by default integrates only the intensity spectra. Change the
    'nst' parameter to integrate the rest of the Stokes parameters if they are present.

    Other parameters are:
      - vsini, projected rotational velocity in km/s, 
      - vrt, radial-tangential velocity in km/s, 
      - resolution, resolution of the output data read from 'filename'
      - rinst, desired instrumental resolution
    """
    with FortranFile(filename) as ff:
        dat = read_bin(ff)

    mu = dat['XMU']
    wlc = dat['WLCNT']
    spc = dat['CNT']
    wll = dat['WL']
    spl = [dat['STOKES'][i] for i in range(nst)]
    
    wl = []
    sp = []
    for spl_ in spl:
        wl_, sp_ = s3div_spectrum(mu, wlc, spc, wll, spl_, vsini,vrt,resol,rinst)
        wl.append(wl_)
        sp.append(sp_)
    return wl, sp