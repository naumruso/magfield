from __future__ import print_function

cimport cython

import numpy as np
cimport numpy as np

#This is how they go as atomic number increases
cdef list __elements = ['H ','He','Li','Be','B ','C ','N ','O ','F ','Ne','Na','Mg',
                        'Al','Si','P ','S ','Cl','Ar','K ','Ca','Sc','Ti','V ','Cr',
                        'Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr',
                        'Rb','Sr','Y ','Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd',
                        'In','Sn','Sb','Te','I ','Xe','Cs','Ba','La','Ce','Pr','Nd',
                        'Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf',
                        'Ta','W ','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po',
                        'At','Rn','Fr','Ra','Ac','Th','Pa','U ','Np','Pu','Am','Cm',
                        'Bk','Cf','Es']

#make a dictionary Z<-->element name
cdef dict __zelem = dict(zip(range(1,100,1),__elements))

cpdef get_zelem(el):
    """returns the atomic number Z of a given element from string"""
    for _zel, _el in __zelem.viewitems():
        if _el.strip().lower() == el.strip().lower():
            return _zel
    return -1
    #raise ValueError("Element \'{:s}\' was not found in the available data.".format(el))

cpdef get_aelem(int zelem):
    """returns the name of the element as string with length two from its atomic number"""
    if zelem > 0 and zelem < 100:
        return __zelem[zelem]
    else:
        return ' X'
        #raise ValueError("There is no element with atomic number {:d} in the available data.".format(zelem))



def parse_zlines(list lines, int nlines, int ilin=0,
                 int npisiz=30, int nsisiz=30,
                 verbose=False):
    """
    Used for quickly parsing the main data block of long format VALD stellar extract data.

    The input arguments are:
      - lines: a Python list of strings,
      - nlines: exact number of spectral lines present in that data,
      - ilin: start from ilin in the Python list 'lines', 
      - npisiz: maximum number of Pi-components for the Zeeman splitting pattern,
      - nsisiz: maximum number of Sigma-components for the Zeeman splitting pattern,
      - verbose: prints useful data in case of exceptions.

    The output is a dictionary with keys that contain all relevant data.
    """
    # main objects where we keep parsed data
    cdef np.ndarray el = np.empty(nlines, 'S2') 
    cdef np.ndarray Z = np.empty(nlines, np.int) #atomic number 
    cdef np.ndarray ion = np.empty(nlines, np.int)
    cdef np.ndarray cwave = np.empty(nlines, np.float64)
    cdef np.ndarray excit = np.empty(nlines, np.float64)
    cdef np.ndarray vmic = np.empty(nlines, np.float64)
    cdef np.ndarray loggf = np.empty(nlines, np.float64)
    cdef np.ndarray rad = np.empty(nlines, np.float64)
    cdef np.ndarray stark = np.empty(nlines, np.float64)
    cdef np.ndarray waals = np.empty(nlines, np.float64)
    cdef np.ndarray lande = np.empty(nlines, np.float64)
    cdef np.ndarray cdepth = np.empty(nlines, np.float64)
    cdef np.ndarray npi = np.empty(nlines, np.int)
    cdef np.ndarray nsigma = np.empty(nlines, np.int)
    cdef np.ndarray spltpi = np.zeros((nlines,npisiz), np.float64)
    cdef np.ndarray strnpi = np.zeros((nlines,npisiz), np.float64)
    cdef np.ndarray spltsi = np.zeros((nlines,nsisiz), np.float64)
    cdef np.ndarray strnsi = np.zeros((nlines,nsisiz), np.float64)
    cdef list rawdata = []

    # temporary variables
    cdef int nlin, j
    cdef object line
    cdef list line_, line__

    for nlin in range(nlines):
        #get current line
        line = lines[ilin]

        #parse the header of the given spectral line
        line_ = line.strip().split(',')
        line__ = line_[0].replace("'","").strip().split()
        el[nlin] = line__[0]
        Z[nlin] = get_zelem(line__[0]) #return atomic number of the element
        ion[nlin] = int(line__[1])
        cwave[nlin] = float(line_[1])
        excit[nlin] = float(line_[2])
        vmic[nlin] = float(line_[3])
        loggf[nlin] = float(line_[4])
        rad[nlin] = float(line_[5])
        stark[nlin] = float(line_[6])
        waals[nlin] = float(line_[7])
        lande[nlin] = float(line_[8])
        cdepth[nlin] = float(line_[9])
        rawdata.append([line]) #this doesn't add a lot of runtime to the subroutine

        #now read the Zeeman line components
        if lande[nlin] < 0.0: #lande components are present for the current line
            #parse first and second lines after the header
            line_ = lines[ilin + 1].split(',')
            line__ = lines[ilin + 2].split(',')
            npi[nlin] = int(line_[0]) #number of Pi components
            for j in range(npi[nlin]):
                spltpi[nlin,j] = float(line_[j+1])
                strnpi[nlin,j] = float(line__[j])
    
            #parse the third and fourth lines after the header
            line_ = lines[ilin + 3].split(',')
            line__ = lines[ilin + 4].split(',')
            nsigma[nlin] = int(line_[0]) #number of Sigma components
            for j in range(nsigma[nlin]):
                spltsi[nlin,j] = float(line_[1+j])
                strnsi[nlin,j] = float(line__[j])
            
            #add the used lines to rawdata for safekeeping
            rawdata[-1] += lines[ilin+1:ilin+5:1]

            #increase the line counter
            ilin += 5 #should be the header of the next spectral line
        else:
            if verbose: print("Except: %2s%2d %12.4f" %(el[nlin],
                                                        ion[nlin],
                                                        cwave[nlin]))
            npi[nlin] = 0
            nsigma[nlin] = 0

            #increase the line counter
            ilin += 1 #should be the header of the next spectral line

    #prepare the output dictionary
    zlines = {'el':el, 'Z':Z, 'ion':ion, 'cwave':cwave, 'excit':excit,
              'vmic':vmic, 'loggf':loggf, 'rad':rad, 'stark':stark,
              'waals':waals, 'lande':lande, 'cdepth':cdepth,
              'npi':npi, 'nsigma':nsigma, 'spltpi':spltpi,
              'strnpi':strnpi, 'spltsi':spltsi, 'strnsi':strnsi,
              'rawdata':rawdata}
    return zlines, ilin


def parse_slines(list lines, int nlines, int ilin=0, verbose=False):
    """
    Used for quickly parsing the main data block of short format VALD stellar extract data.

    The input arguments are:
      - lines: a Python list of strings,
      - nlines: exact number of spectral lines present in that data,
      - ilin: start from ilin in the Python list 'lines', 
      - verbose: does nothing here.

    The output is a dictionary with keys that contain all relevant data.
    """
    # main objects where we keep parsed data
    cdef np.ndarray el = np.empty(nlines, 'S2')
    cdef np.ndarray Z = np.empty(nlines, np.int)
    cdef np.ndarray ion = np.empty(nlines, np.int)
    cdef np.ndarray cwave = np.empty(nlines, np.float64)
    cdef np.ndarray excit = np.empty(nlines, np.float64)
    cdef np.ndarray vmic = np.empty(nlines, np.float64)
    cdef np.ndarray loggf = np.empty(nlines, np.float64)
    cdef np.ndarray rad = np.empty(nlines, np.float64)
    cdef np.ndarray stark = np.empty(nlines, np.float64)
    cdef np.ndarray waals = np.empty(nlines, np.float64)
    cdef np.ndarray lande = np.empty(nlines, np.float64)
    cdef np.ndarray cdepth = np.empty(nlines, np.float64)

    # temporary variables
    cdef int nlin, j
    cdef object line
    cdef list line_, line__

    for nlin in range(nlines):
        #parse the header of the current spectral line
        line_ = lines[ilin].strip().split(',')
        line__ = line_[0].replace("'","").strip().split()
        el[nlin] = line__[0]
        Z[nlin] = get_zelem(line__[0]) #return atomic number of the element
        ion[nlin] = int(line__[1])
        cwave[nlin] = float(line_[1])
        excit[nlin] = float(line_[2])
        vmic[nlin] = float(line_[3])
        loggf[nlin] = float(line_[4])
        rad[nlin] = float(line_[5])
        stark[nlin] = float(line_[6])
        waals[nlin] = float(line_[7])
        lande[nlin] = float(line_[8])
        cdepth[nlin] = float(line_[9])

        #increase the line counter
        ilin += 1 #should be the header of the next spectral line

    #prepare the output dictionary
    result = {'el':el, 'Z':Z, 'ion':ion, 'cwave':cwave, 'excit':excit,
              'vmic':vmic, 'loggf':loggf, 'rad':rad, 'stark':stark,
              'waals':waals, 'lande':lande, 'cdepth':cdepth}
    return result, ilin


