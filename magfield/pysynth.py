from __future__ import print_function, absolute_import, division

import tempfile
import subprocess
import copy
import time

import numpy as np
from scipy import optimize
from scipy import interpolate

from .pyvald import print_atable, print_zheader, read_zfile, get_zelem
from .pys3div import s3div_bin


def std_deviation(solution):
    """
    Estimate the standard deviation of the minimized
    parameters returned by the scipy Levenberg-Marquardt function.

    Pass the complete solution to get the error estimates, i.e.,
    set the keyword argument full_output=1 on input to scipy.optimize.leastsq.
    """
    (popt, pcov, infodict, errmsg, ier) = solution

    if pcov is None:
        pcov = np.zeros((len(popt), len(popt)), dtype=np.float64)
        pcov.fill(np.inf)
    else:
        sig_sqr = np.sum(infodict['fvec']**2) / \
                         (infodict['fvec'].size - len(popt))

    return np.sqrt(pcov.diagonal() * sig_sqr)


class MagSynth(object):
    """
    The class is used in combination with long format VALD stellar
    extract output and the magnetic synthesis code Synmast to compute
    synthetic spectra with magnetic fields and to fit observations. 

    The fitting procedures as they are implemented now can only fit
    the abundance of a selection of chemical elements, but it is not
    too difficult to implement fitting of other static parameters
    such as vsini, vmacro, or a magnetic field.
    """
    def __init__(self, zdata, vsini=None, vmacro=None, Resol=None):
        """
        To initiate the class you need to pass the 'zdata' argument
        which is the output of pyvald.read_zfile or pyvald.read_zlist
        procedures. This sets up all default values of the calculations.

        Additional arguments can be vsini, vmacro, or Resol, which 
        correspond to the projected rotational velocity (km/s), the macro
        turbulent velocity (km/s), and the instrumental resolution.
        """
        # copy the contents of the line list
        # the format of the input is what we get from pyvald.read_zfile
        # It contains everything we need to compute synthetic magnetic
        # spectra with Synmast.
        self.zdata = copy.deepcopy(zdata)

        # copy default data from zelf.zdata
        self.default = {} # this never changes once entered
        self.default['atable'] = self.zdata['atable']
        self.default['B'] = np.asarray(self.zdata['header']['mfield'], dtype=np.int32)
        self.default['wstart'] = self.zdata['header']['wstart']
        self.default['wend'] = self.zdata['header']['wend']
        self.default['krz'] = self.zdata['krz']
        self.default['cwave'] = self.zdata['sp_lines']['cwave']

        # open the atm. model file, read its contents,
        # and save them to a temporary file
        # this is done once because we assume there's no
        # need to change the atmosphere model in one same
        # instance of the class.
        with open(self.default['krz']) as fin:
            lines = fin.readlines()
        self.default['atmmodel'] = tempfile.NamedTemporaryFile()
        self.default['atmmodel'].file.writelines(lines)
        self.default['atmmodel'].file.flush()

        # parameters related to synmast
        if vsini is not None:
            self.default['vsini'] = float(vsini)
        else:
            self.default['vsini'] = 0.0

        if vmacro is not None:
            self.default['vmacro'] = float(vmacro)
        else:
            self.default['vmacro'] = 0.0

        if Resol is not None:
            self.default['Resol'] = int(Resol)
        else:
            self.default['Resol'] = 1000000

        # initiate the writeable parameters
        # these are the current values that
        # change from run to run, this is what
        # we use to compute chunks of the spectrum
        self.current = {}

        # zdata parameters
        self.current['B'] = self.default['B']
        self.current['wstart'] = self.default['wstart']
        self.current['wend'] = self.default['wend']
        self.current['atable'] = self.default['atable'].copy()

        # s3div parameters (disk integration and convolution)
        self.current['vsini'] = self.default['vsini']
        self.current['vmacro'] = self.default['vmacro']
        self.current['Resol'] = self.default['Resol']

        # container for the spectra
        self.syn = {'wl': None, 'sp': None}
        self.obs = {'wl': None, 'sp': None}

        # container with info for the fitting method
        self.fitting = None

    def update_mfield(self, B):
        """
        Updates the write values for the magnetic field.
        The original default values are still kept and
        can be accessed as self.default['B'].
        """
        assert len(B) == 3
        self.current['B'] = np.asarray(B, dtype=np.int32)

    def restore_mfield(self):
        """restores the default values of the mag. field"""
        self.current['B'] = np.asarray(self.default['B'], dtype=np.int32)

    def update_wstart(self, wstart):
        """
        Updates the write values for wstart.
        The original default value is kept as self._wstart
        """
        if self.default['wstart'] <= wstart:
            self.current['wstart'] = float(wstart)
        else:
            raise ValueError("wstart is beyond the wavelength range")

    def restore_wstart(self):
        """restores the default value for wstart"""
        self.current['wstart'] = self.default['wstart']

    def update_wend(self, wend):
        """
        Updates the write values for wend.
        The original default value is kept as self._wend
        """
        if self.default['wend'] >= wend:
            self.current['wend'] = float(wend)
        else:
            raise ValueError("wend is beyond the wavelength range")

    def restore_wend(self):
        """restores the default values of the wavelength range"""
        self.current['wend'] = self.default['wend']

    def update_abundance(self, zelem, ab):
        """
        Updates the current abundance table for one element.
        The default abundance table is kept in self._atable
        """
        self.current['atable'][zelem] = float(ab)

    def restore_atable(self):
        """restores the initial abundance table"""
        self.current['atable'] = self.default['atable'].copy()

    def new_zdata(self):
        """
        Make new VALD line list according to the
        parameters that are present in self.current.

        The resulting Python list is ready to be printed
        in a file as an ascii data.
        """
        # select the relevant lines from the line list
        wstart_idx = self.default['cwave'] >= self.current['wstart']
        wend_idx = self.default['cwave'] <= self.current['wend']
        idx, = np.where(wstart_idx & wend_idx)
        zlines_print = []
        for i in idx:
            zlines_print += self.zdata['sp_lines']['rawdata'][i]

        # create the header of the new line list
        zheader = copy.deepcopy(self.zdata['header'])
        zheader['mfield'] = self.current['B']
        zheader['wstart'] = self.current['wstart']
        zheader['wend'] = self.current['wend']
        zheader['nlines_sel'] = len(idx)
        zdata_new = print_zheader(zheader)

        # append the selected spectral lines
        zdata_new += zlines_print

        # copy the model atmosphere file to the new line list
        zdata_new.append("\'{}\',\n".format(self.default['atmmodel'].name))

        # append the abundance table
        zdata_new += print_atable(self.current['atable'])

        return zdata_new

    def __call__(self, wstart=None, wend=None, B=None, atable=None,
                 vsini=None, vmacro=None, Resol=None, verbose=False):
        """
        Call this method to compute the spectrum.

        Some of the input arguments can override the default values
        that are read from the input line list during the init.

        The arguments are:
          - wstart: start of the wavelength region (in Angstrom),
          - wend: end of the wavelength region (in Angstrom),
          - B: magnetic field vector [Br, Bm, Ba],
          - atable: abundance table (Python dict {'El': Value}),
          - vsini: projected rotational velocity (in km/s),
          - vmacro: macroscopic turbulent velocity (in km/s),
          - Resol: desired instrumental resolution of the output data.

        The use can control the output with the argument 'verbose'.

        The results of the calculations are returned as two numpy arrays,
        which can also be found in self.syn['wl'] and self.syn['sp'].
        """
        # restore the default values
        self.restore_wstart()
        self.restore_wend()
        self.restore_mfield()
        self.restore_atable()

        # restore s3div default parameters
        # defaults are passed to the code at __init__
        self.current['vsini'] = self.default['vsini']
        self.current['vmacro'] = self.default['vmacro']
        self.current['Resol'] = self.default['Resol']

        # update synmast parameters
        # these are used to make the new zfile
        if wstart is not None:
            self.update_wstart(wstart)
        if wend is not None:
            self.update_wend(wend)
        if B is not None:
            self.update_mfield(B)

        # update the default abundance table with new values
        if atable is not None:
            for item in atable.items():
                self.update_abundance(*item)

        # update s3div parameters
        if vsini is not None:
            self.current['vsini'] = float(vsini)
        if vmacro is not None:
            self.current['vmacro'] = float(vmacro)
        if Resol is not None:
            self.current['Resol'] = int(Resol)

        # write to temporary file the contents of the synmast input file
        zdata = self.new_zdata() #uses current dict for that
        zfile = tempfile.NamedTemporaryFile()
        zfile.file.writelines(zdata)
        zfile.file.flush()

        # open temporary file for the synmast output file
        binfile = tempfile.NamedTemporaryFile()

        # call synmast_bin
        if verbose:
            subprocess.call(['synmast_bin', zfile.name, binfile.name])
        else:
            subprocess.check_output(['synmast_bin', zfile.name, binfile.name])

        # disk integration and convolution with instrumental profile
        wl, sp = s3div_bin(binfile.name, vsini=self.current['vsini'], resol=1e6,
                           vrt=self.current['vmacro'], rinst=self.current['Resol'], nst=1)
        self.syn['wl'] = wl[0]
        self.syn['sp'] = sp[0]

        # close temp files
        zfile.file.close()
        binfile.file.close()

        return self.syn['wl'], self.syn['wl']

    def setup_fit(self, wl, sp, elements,
                  weights=None, B=None, bounds=None, atable=None,
                  vsini=None, vmacro=None, Resol=None,
                  dwl=0.5, interp='linear'):
        """
        A helper method that copies supplied data and prepares 
        a number of internal variables for fitting a synthetic
        spectrum to observed data. We need to call this before
        running any of the fitting methods.

        The input arguments are:
          - wl: wavelength array of observations,
          - sp: observed spectrum at every wl point,
          - elements: a Python list of elements that we're fitting,
          - weights: array of weights for each point in wl,
          - B: magnetic field vector [Br, Bm, Ba],
          - bounds: constrain the abundance values of the fitted elements,
          - atable: custom abundance table,
          - vsini: projected rotational velocity,
          - vmacro: macroscopic turbulent velocity,
          - Resol: resolution of the observed data,
          - dwl: expands the wavelength ranges by dwl during the fitting,
          - interp: Not implemented.
        """
        # observations
        self.obs['wl'] = np.asarray(wl, dtype=np.float64)
        self.obs['sp'] = np.asarray(sp, dtype=np.float64)

        # elements that we're fitting
        self.fitting = dict(elements=elements)

        # define the weights for the pixels in the spectrum
        if weights is None:
            self.fitting['weights'] = np.ones(len(wl))
        else:
            self.fitting['weights'] = np.asarray(weights, dtype=np.float64)

        # updates the default value read from the zlist
        self.fitting['B'] = B

        # update the abundance table stored in self.current
        self.fitting['atable'] = {}
        if atable is not None:
            for el, ab in atable.items():
                zelem = get_zelem(el)
                self.fitting['atable'][zelem] = float(ab)

        # update the macroscopic parameters
        # these are passed to s3div
        self.fitting['vsini'] = vsini
        self.fitting['vmacro'] = vmacro
        self.fitting['Resol'] = Resol

        # this sets the constrants for the minimization procedure
        # the abundance values will be betweeen -20 and -1
        if bounds is None:
            self.fitting['bounds'] = [(-20.0, -1.0)]*len(elements)

        # interpolation type for the interpolation of the synthetic
        # spectrum onto the observed wavelength grid
        # self.fitting['interp'] = interp

        # when computing the synthetic spectrum expand the wavelength
        # limits by dwl. This is done to avoid any problems during the
        # integration and interpolation of the synthetic spectrum.
        self.fitting['dwl'] = dwl

    def chisq(self, abundances, vector=False, verbose=False):
        """
        Computes the values of the chisq function for the data
        inputted by running the method self.setup_fit.

        The argument 'abundances' is a list/array of values for which
        we are computing the current chisq values.

        In case the argument 'vector' is set to True, then the
        method returns the vector of values:

            {weights * (observed data - computed data)}_i.

        The return data can also be accessed in self.fitting['chisq']
        and self.fitting['chisq_vec'].
        """
        # run checks
        if self.fitting is None:
            raise ValueError("You need first to run self.setup_fit.")

        assert len(self.fitting['elements']) == len(abundances)

        # update the abundance table with the new values
        for el, ab in zip(self.fitting['elements'], abundances):
            zelem = get_zelem(el)
            self.fitting['atable'][zelem] = float(ab)

        # check that the new values satisfy the constraints
        # We do this only for the elements that we fit
        items = zip(self.fitting['elements'],
                    abundances,
                    self.fitting['bounds'])
        for el, abn, bound in items:
            if abn < (bound[0]-0.5) or abn > (bound[1] + 0.5):
                err_str = "Abundance({0:6.2f}) for {1:2s} is not betweeen \
                           ({2[0]:6.2f},{2[-1]:6.2f})."
                raise ValueError(err_str.format(abn, el, bound))

        # run the calculations
        tstart = time.time()
        self.__call__(wstart=self.obs['wl'][0] - self.fitting['dwl'],
                      wend=self.obs['wl'][-1] + self.fitting['dwl'],
                      B=self.fitting['B'],
                      atable=self.fitting['atable'],
                      vsini=self.fitting['vsini'],
                      vmacro=self.fitting['vmacro'],
                      Resol=self.fitting['Resol'])

        # interpolate the synthetic spectrum on the wavelength grid of 
        # the observations. Linear interpolation is enough as long as the
        # synthetic spectra have artificial resolution >= 1e6.
        self.syn['sp'] = np.interp(self.obs['wl'], self.syn['wl'], self.syn['sp'])
        self.syn['wl'] = self.obs['wl']

        # calculate the chisquared vector
        chisq_vec = self.fitting['weights']*(self.obs['sp'] - self.syn['sp'])
        self.fitting['chisq_vec'] = chisq_vec
        self.fitting['chisq'] = np.sum(chisq_vec**2)

        if verbose:
            # prepare time string
            tend = time.time()
            m, s = divmod(tend - tstart, 60)
            h, m = divmod(m, 60)
            tstr = "dTime={:02.0f}:{:02.0f}:{:04.1f}".format(h, m, s)

            # prepare vector string
            vstr = "x=({0:2s}:{1:6.2f}".format(self.fitting['elements'][0],
                                               abundances[0])
            for i in range(1, len(abundances), 1):
                vs = ", {0:2s}:{1:6.2f}".format(self.fitting['elements'][i],
                                                abundances[i])
                vstr += vs
            vstr += ")"

            # prepare chisq string
            cstr = "Chisq={:9.5e}".format(self.fitting['chisq'])

            # print the info string
            print(tstr + ' - ' + cstr + ' - ' + vstr)

        # return what was requested by the caller
        # by default it returns the chisq value
        # the vector is used by optimize.leastsq
        if vector:
            return self.fitting['chisq_vec']
        else:
            return self.fitting['chisq']

    def fit_leastsq(self, x0, **kwargs):
        """
        Performs non-linear least squares fit to observed data
        by fitting the abundance of a given chemical element. 

        Note that we must run self.setup_fit before we can use
        this method.

        The argument x0 is the array with the inital values
        of the fitted parameters.
        
        This method calls the following scipy function:
        
        optimize.leastsq(func, x0, args=(), Dfun=None,
            full_output=0, col_deriv=0, ftol=1.49012e-08, xtol=1.49012e-08,
            gtol=0.0, maxfev=0, epsfcn=None, factor=100, diag=None)

        The arguments to the scipy minimization function are
        passed as **kwargs.
        """
        x0 = np.asarray(x0, dtype=np.float64)
        func = self.chisq
        options = dict(full_output=1,
                       epsfcn=1e-04)
        options.update(**kwargs)
        if 'verbose' in options:
            verbose = options.pop('verbose')
        else:
            verbose = False
        return optimize.leastsq(func, x0, args=(True, verbose), **options)

    def fit_simplex(self, x0, **kwargs):
        """
        Use the Simplex method to perform minimization of the
        chisq function to fit observed data by adjusting the abundance
        value of a given chemical element. 

        Note that we must run self.setup_fit before we can use
        this method. 

        The argument x0 is the array with the inital values
        of the fitted parameters.
        
        This method calls the following scipy function:
        
        optimize.fmin(func, x0, args=(),
            xtol=0.0001, ftol=0.0001, maxiter=None, maxfun=None,
            full_output=0, disp=1, retall=0, callback=None)

        Use **kwargs to pass arguments to the scipy minimization function.
        """
        x0 = np.asarray(x0, dtype=np.float64)
        func = self.chisq
        options = dict(xtol=0.01,
                       ftol=0.01,
                       full_output=1,
                       disp=5)
        options.update(**kwargs)
        return optimize.fmin(func, x0, **options)

    def fit_tnc(self, x0, **kwargs):
        """
        Use a version of the truncated Newthon method to minimize the
        chisq function to fit observed data by adjusting the abundance
        value of a given chemical element. 

        Note that we must run self.setup_fit before we can use
        this method. 

        The argument x0 is the array with the inital values
        of the fitted parameters.

        This method calls the following scipy function:
        
        optimize.fmin_tnc(func, x0, fprime=None, args=(),
            approx_grad=0, bounds=None, epsilon=1e-08, scale=None,
            offset=None, messages=15, maxCGit=-1, maxfun=None, eta=-1,
            stepmx=0, accuracy=0, fmin=0, ftol=-1, xtol=-1, pgtol=-1,
            rescale=-1, disp=None, callback=None)

        The arguments to the scipy minimization function are passed through **kwargs.
        """
        x0 = np.asarray(x0, dtype=np.float64)
        func = self.chisq
        options = dict(bounds=self.fitting['bounds'],
                       approx_grad=True,
                       epsilon=1e-02,
                       accuracy=1e-03,
                       xtol=1e-01,
                       ftol=1e-01,
                       disp=5)
        options.update(kwargs)
        return optimize.fmin_tnc(func, x0, **options)

    def fit(self, x0, method, **kwargs):
        """
        Start the fitting method.
        
        The method used for the minimization of the chisq function can be:
        
        - 'leastsq' (Levenberg-Marquardt minimization, calculates gradients)
        - 'simplex' (Simplex method minimization, only funcation evaluations)
        - 'tnc' (Truncated Newton method with bounds for the parameters)
        
        Passes arguments to the scipy minimization function through **kwargs.

        The argument x0 is the array with the initial values of the fitted parameters.

        Note that we need first to run self.setup_fit before we can run this method.
        """
        if method.lower().strip() == 'leastsq':
            return self.fit_leastsq(x0, **kwargs)
        elif method.lower().strip() == 'simplex':
            return self.fit_simplex(x0, **kwargs)
        elif method.lower().strip() == 'tnc':
            return self.fit_tnc(x0, **kwargs)
        else:
            raise ValueError("Method %s not recognised." %method)
