import os
import copy
import shutil
import subprocess32 as subprocess
import tempfile
import itertools
import multiprocessing
import time

import numpy as np
from scipy import interpolate, optimize

from magfield import pyvald, pykrz, pys3div

def radvelcorr(wave, radvel):
    """
    Shift the wavelength array wave for a given radial velocity
    """
    return wave * (1.0 + radvel / 2.9979246e5)

def stddev_leastsq(solution):
    """
    Estimate the standard deviation of the minimization
    parameters returned by the Levenberg-Marquiardt method.

    Pass the full solution (set full_output=1 on input to optimize.leastsq)
    to get the error estimates.
    """
    (popt, pcov, infodict, errmsg, ier) = solution

    if pcov is None:
        pcov = np.zeros((len(popt), len(popt)), dtype=np.float64)
        pcov.fill(inf)
    else:
        sig_sqr = np.sum(infodict['fvec']**2) / (infodict['fvec'].size - len(popt))

    return np.sqrt(pcov.diagonal() * sig_sqr)

def cut_wl(wl, wstart, wend):
    idx, = np.where((wl >= wstart) & (wl <= wend))
    return idx

def bracket(func, y1, yopt, x1, xbounds, step, args=(), nmax=10,
            xtol=None, atol=1e-3, rtol=0.02, verbose=False):
    """
    Find the value of xopt such that func(xopt) ~= yopt.

    y2 = func(x2) is the initial guess.

    The search is done for values of x in [xbounds[0], xbounds[1]]

    xtol is the minimal distance between values of x for two consecutive
    iterations. If xtol is None, the program adopts the initial value of
    step as xtol.

    The search proceeds as long as the difference between value of y for
    two consecutive iterations is more than atol + rtol*|ynew|, or until
    the max. number of iterations has been reached (nmax).

    This function is called by the method VertStrat.asymmetric_errorbars()
    """
    #minimal change in x, step can be negative
    if xtol is None: xtol = np.abs(step)

    #make sure x1 is between xbounds[0] and xbounds[1]
    if x1 < xbounds[0] or xbounds[1] < x1:
        if verbose: print("Initial value for x1 is not between the supplied xmin and xmax values.")
        return x1, y1

    #bracket the function value, so that y1 < yopt < y2
    coef = 1
    niter = 0
    while True:
        #new x value
        x2 = x1 + coef * step

        #if we're overshooting set x2 to the maximum/minimum possible value
        if xbounds[0] > x2:
            x2 = xbounds[0]
        elif xbounds[1] < x2:
            x2 = xbounds[1]

        #new y value
        y2 = func(x2, *args)

        #check stopping conditions
        cond1 = y2 < yopt
        cond2 = niter < nmax
        cond3 = xbounds[0] < x2 and x2 < xbounds[1]

        if cond1 and cond2 and cond3:
            #prepare for next step
            coef *= 2
            niter += 1

            #new is now old
            x1 = x2
            y1 = y2
        else:
            break

    #check what failed
    if not cond2:
        if verbose: print("Maximum number of iterations achieved.")
        return x2, y2

    if not cond3 and cond1:
        if verbose: print("For all x in [xmin, xmax] we have func(x) < yopt.")
        return x2, y2


    #now these conditions are satisfied
    #x1 <= xopt <= x2
    #y1 <= yopt <= y2
    while True:
        #linear search for xnew; y = a*x + b
        a = (y1 - y2) / (x1 - x2)
        b = 0.5 * (y1 + y2) - 0.5 * a * (x1 + x2)
        xnew = (yopt - b) / a

        #evaluate function at xnew
        ynew = func(xnew, *args)

        #adopt new x1, x2 and y1, y2
        if ynew < yopt:
            x1 = xnew
            y1 = ynew
        else:
            x2 = xnew
            y2 = ynew

        #check stopping conditions
        cond1 = np.allclose(y1, y2, atol=atol, rtol=rtol)
        cond2 = niter < nmax
        cond3 = np.abs(x1 - x2) > xtol #difference between two consecutive x values
        if not cond1 and cond2 and cond3:
            #increase iterator
            niter += 1
        else:
            break

    #see what failed
    if cond1:
        if verbose: print("Difference between two consecutive y values is less than atol + rtol*|y2|.")
    if not cond2:
        if verbose: print("Maximum number of iterations reached.")
    if not cond3:
        if verbose: print("Difference between two consecutive x values is less than xtol.")
    return xnew, ynew


class Worker(object):
    """
    A Worker class used for computing synthetic spectra by an instance of VertStrat
    or anything else for that matter. This class uses the subprocess module and
    needs synmast_bin to be available in the system bin path. In a way, it's just
    a fancy wrapper for synmast_bin that cleanly runs the calculations, reads the
    results, removes the temp files, and processes the final results.
    """
    def __init__(self, krzfile='input.krz'):
        """
        krzfile tells the Worker class instance what is the filename of the
        krz model atmosphere, which will be later specified in zprint (see the
        self.synmast() method).

        You can change self.krzfile after instantiating the class.

        Change the variables synmast and s3div if the executables need to be
        accessed in a different way.
        """
        self.tmpdir = tempfile.mkdtemp() #do the calculations in self.tmpdir
        self.zfile = os.path.join(self.tmpdir, 'input.z') #filename of the Zeeman synmast input file
        self.krzfile = os.path.join(self.tmpdir, krzfile) #filename of the krz model atmosphere
        self.binfile = os.path.join(self.tmpdir, 'output.10') #synmast output file

        self.process = None
        self.wl = None
        self.sp = None
        self.info = None

    def synmast(self, zprint, krzprint, info=None):
        """
        zprint is a list of strings of a zlist ready to be printed to a file.
        zprint should be the output of the function print_zlist

        krzprint is a list of strings of a krz model atmosphere file ready to
        be printed to a file.
        krzprint should be the output of pykr.print_krz

        info can be a python object that I use to store temporary information.
        """
        with open(self.zfile, 'w') as fout:
            fout.writelines(zprint)
        #fout = open(self.zfile, 'w')
        #fout.writelines(zprint)
        #fout.close()

        with open(self.krzfile, 'w') as fout:
            fout.writelines(krzprint)
        #fout = open(self.krzfile, 'w')
        #fout.writelines(krzprint)
        #fout.close()

        #start the synmast calculations in self.tmpdir
        self.process = subprocess.Popen(['synmast_bin', self.zfile,
                                        self.binfile], cwd=self.tmpdir,
                                        stdout=subprocess.PIPE)
        self.info = info #variable used by the master (dict, list, tuple, etc.)

        #reset the variables in case self.s3div is called before synmast
        #has finished calculating the synmthetic profiles.
        self.wl = None
        self.sp = None

    def done(self):
        """Check is synmast is still doing the calculations"""
        return self.process.poll() is not None

    def success(self):
        """If synmast exits abnormally self.success() returns False"""
        return self.process.returncode == 0

    def s3div(self, vsini=0.0, vmacro=0.0, resol=0.0):
        """
        Once synmast is done calculating the spectra, proceed with disk
        integration and convolution with a Gaussian to account for the
        finite spectral resolution of the observations and the non zero
        vsini of the object.

        The resulting synthetic spectra are stored in self.wl and self.sp.

        It should be called after synmast has done calculating the spectra.
        In the opposite case it does nothing, self.wl and self.sp will
        contain None.

        This is a new version that uses the s3div Fortran code compiled
        as a runtime library. It should be more accurate and faster.
        """
        if self.process is None: return

        # block until done
        if not self.done():
            self.process.wait()
        
        wl, sp = pys3div.s3div_bin(self.binfile, vsini=vsini, vrt=vmacro,
                                   rinst=resol, nst=1)
        self.wl = wl[0]
        self.sp = sp[0]

    def delete(self):
        """
        Delete the temporary directory used for the calculations.

        Wait in case there is something else running there.
        """
        if self.process is not None:
            self.process.wait()

        shutil.rmtree(self.tmpdir)


def vprofile_homog(*args, **kwargs):
    """
    This is a simple helper function used for fitting
    homogeneous abundance. It returns a function that returns
    a constant value (eps_u) for all input values.

    The input should be args = (lgrhox, eps_u, eps_l, d0, Delta).

    The only variable we care about is eps_u, all other
    parameters are just here for continuity sake with other functions.
    """
    eps_u = args[1]

    verbose = kwargs['verbose']
    if verbose:
        print("VProfileFunc: %7.4f,%7.4f,%7.4f,%7.4f" %(eps_u, 0.0, 0.0, 0.0))

    return [],[],np.poly1d([eps_u])


def vprofile_hpp(*args, **kwargs):
    """
    A function that finds the abundance as a function of lgrhox
    with the help of Hermitian Piecewise Polynomials.

    Input parameters:
        args = (lgrhox, eps_u, eps_l, d0, Delta)
        kwargs = dict(nlayers=None, deltanlayers=None)

    lgrhox: log10 of the rhox array, gives the density
            as it changes with vertical depth
    eps:    abundance as a function of lgrhox

    eps_u:  abundance at the top layers of the atmosphere
    eps_l:  abundance at the bottom layers of the atmosphere
    d0:     position of the center of the transiton window
    Delta:  width of the transiton window

    Scheme of the vertical stratification profile:

      eps ^
          |                 eps_l
    eps_l x              ------------
          |             /^          ^
          |            / |          |
          |           /  |          |
          |          /^  |          |
          | eps_u   / |  |          |
    eps_u x--------/ d0  |          |
          |^       ^     |          |
          ||       |     |          |
          ||       |Delta|          |
          -x-------x-----x----------x--> lgrhox
           a       b     c          d

    This function tries to find a Hermitian Piecewise Polynomial (HPP)
    from the four points:
        (a, eps_u)
        (b, eps_u)
        (c, eps_l)
        (d, eps_l)

    The advantage of using such polynomial is that it preserves monotonicity
    between the points and it produces smooth solution.

    In the general case we compute the position of the points on the x-axis as:
        a = lgrhox[0]
        b = d0 - Delta/2
        c = d0 + Delta/2
        d = lgrhox[-1].

    In order to successfully find a non degenerate polynomial the points on the
    x-axis must satisfy the condition a < b < c < d. Of particular interest
    are the points b and c. To solve that, the user is expected to supply a
    function Deltafunc that returns the minimum distance between the points as
    a function of d0 with proper management if d0 is outside the range of lgrhox.

    The function returns
    xpars:  [a,b,c,d]
    ypars:  [eps_u, eps_u, eps_l, eps_l]
    pint:   the found polynomial, callable
    """
    #extract positional arguments
    [lgrhox, eps_u, eps_l, d0, Delta] = args

    #extract keyword arguments
    Deltafunc = kwargs['Deltafunc']
    verbose = kwargs['verbose']

    #Check if Delta is too small for d0
    #This matters only when d0 is within [d0min, d0max]
    Delta_min = Deltafunc(d0)
    if not np.isnan(Delta_min):
        if Delta < Delta_min:
            print("Warning: VProfileFunc: Delta {:7.4f} < Delta_min {:7.4f}".format(Delta, float(Delta_min)))
            Delta = float(Delta_min)

    #Conditions to satisfy
    #x0 < x1 < x2 < x3
    x0, y0 = (lgrhox[0], eps_u)
    x1, y1 = (d0 - Delta/2., eps_u)
    x2, y2 = (d0 + Delta/2., eps_l)
    x3, y3 = (lgrhox[-1], eps_l)

    xpars = [x0,x1,x2,x3]
    ypars = [y0,y1,y2,y3]

    #check the last two points
    if x0 > x1: xpars[0] = x1 - 0.5
    if x3 < x2: xpars[3] = x2 + 0.5

    if verbose:
        print("VProfileFunc: {0[0]:7.4f},{0[1]:7.4f},{0[2]:7.4f},{0[3]:7.4f}".format(xpars))

    # the interpolating polynomial
    pint = interpolate.pchip(xpars, ypars)
    return xpars, ypars, pint


def vprofile_lip(*args, **kwargs):
    """
    A function that finds the abundance as a function of lgrhox
    with a linear interpolating polynomial from 4 points, and
    uses np.interpolate for the actual interpolation.

    Input parameters:
        args = (lgrhox, eps_u, eps_l, d0, Delta)
        kwargs = dict(nlayers=None, deltanlayers=None)

    lgrhox: log10 of the rhox array, gives the density
            as it changes with vertical depth
    eps:    abundance as a function of lgrhox

    eps_u:  abundance at the top layers of the atmosphere
    eps_l:  abundance at the bottom layers of the atmosphere
    d0:     position of the center of the transiton window
    Delta:  width of the transiton window

    Scheme of the vertical stratification profile:

      eps ^
          |                 eps_l
    eps_l x              ------------
          |             /^          ^
          |            / |          |
          |           /  |          |
          |          /^  |          |
          | eps_u   / |  |          |
    eps_u x--------/ d0  |          |
          |^       ^     |          |
          ||       |     |          |
          ||       |Delta|          |
          -x-------x-----x----------x--> lgrhox
           a       b     c          d

    This function tries to find a Linear Interpolating Polynomial (LIP)
    from the four points:
        (a, eps_u)
        (b, eps_u)
        (c, eps_l)
        (d, eps_l)

    The advantage of using such polynomial is that it preserves monotonicity
    between the points.

    In the general case we compute the position of the points on the x-axis as:
        a = lgrhox[0]
        b = d0 - Delta/2
        c = d0 + Delta/2
        d = lgrhox[-1].

    In order to successfully find a non degenerate polynomial the points on the
    x-axis must satisfy the condition a < b < c < d. Of particular interest
    are the points b and c. To solve that, the user is expected to supply a
    function Deltafunc that returns the minimum distance between the points as
    a function of d0 with proper management if d0 is outside the range of lgrhox.

    The function returns
    xpars:  [a,b,c,d]
    ypars:  [eps_u, eps_u, eps_l, eps_l]
    pint:   the found polynomial, callable
    """
    #extract positional arguments
    [lgrhox, eps_u, eps_l, d0, Delta] = args

    #expand the old grid just a bit
    newlgrhox = np.copy(lgrhox)
    newlgrhox[0] -= 1.0
    newlgrhox[-1] += 1.0

    #extract keyword arguments
    Deltafunc = kwargs['Deltafunc']
    verbose = kwargs['verbose']

    #Check if Delta is too small for d0. Important to avoid crashes of synmast
    aDelta = np.abs(Delta)
    Delta_min = Deltafunc(d0)
    if not np.isnan(Delta_min):
        if aDelta < Delta_min:
            print("Warning: VProfileFunc: Delta {:7.4f} < Delta_min {:7.4f}".format(aDelta, float(Delta_min)))
            aDelta = float(Delta_min)

    #Conditions to satisfy
    #x0 < x1 < x2 < x3
    x0, y0 = (lgrhox[0]+0.05, eps_u)
    x1, y1 = (d0 - aDelta/2., eps_u)
    x2, y2 = (d0 + aDelta/2., eps_l)
    x3, y3 = (lgrhox[-1]-0.05, eps_l)

    xpars = [x0,x1,x2,x3]
    ypars = [y0,y1,y2,y3]

    #check the last two points
    #this will fail under certain circumstances with the new method
    #if x0 > x1: xpars[0] = x1 - 0.5
    #if x3 < x2: xpars[3] = x2 + 0.5

    #the new model can't really deal with these things now
    xpars = [x0, (2*x0+x3)/3, (x0+2*x3)/3, x3]

    if verbose:
        print("VProfileFunc: {0[0]:7.4f},{0[1]:7.4f},{0[2]:7.4f},{0[3]:7.4f}".format(xpars))

    # the interpolating polynomial
    #pint = interpolate.interp1d(xpars, ypars, kind='linear', assume_sorted=True)
    #return xpars, ypars, pint

    abund = np.zeros(newlgrhox.size, dtype=np.float64)

    #lower abundance
    idx, = np.where(newlgrhox < d0-aDelta/2.)
    abund[idx] = eps_u

    #upper abundance
    idx, = np.where(newlgrhox > d0+aDelta/2.)
    abund[idx] = eps_l

    #the abundance between them
    stepfunc = interpolate.interp1d([d0-aDelta/2., d0+aDelta/2.], [eps_u, eps_l])
    idx, = np.where((newlgrhox > d0-aDelta/2.) & (newlgrhox < d0+aDelta/2.))
    abund[idx] = stepfunc(newlgrhox[idx])

    #create the relevant interpolation function
    fint = interpolate.interp1d(newlgrhox, abund, kind='linear')
    return xpars, fint(xpars), fint

def vprofile_lip3p(*args, **kwargs):
    """
    A function that finds the abundance as a function of lgrhox
    with linear piecewise polynomial from 3 points.

    Input parameters:
        args = (lgrhox, eps_u, eps_l, d0)
        kwargs = dict(nlayers=None, deltanlayers=None)

    lgrhox: log10 of the rhox array, gives the density
            as it changes with vertical depth
    eps:    abundance as a function of lgrhox

    eps_u:  abundance at the top layers of the atmosphere
    eps_l:  abundance at the bottom layers of the atmosphere
    d0:     position of the center of the transiton window
    Delta:  width of the transiton window

    Scheme of the vertical stratification profile:

      eps ^
          |
    eps_l x              -
          |             /|
          |            / |
          |           /  |
          |          /   |
          |         /    |
    eps_u x--------/     |
          |^       ^     |
          ||       |     |
          ||       |     |
          -x-------x-----x------------> lgrhox
           a       b     c

    This function tries to find a Linear Interpolating Polynomial (LIP)
    from the four points:
        (a, eps_u)
        (b, eps_u)
        (c, eps_l)

    In the general case we compute the position of the points on the x-axis as:
        a = lgrhox[0]
        b = d0
        c = lgrhox[-1].

    In order to successfully find a non degenerate polynomial the points on the
    x-axis must satisfy the condition a < b < c. Of particular interest
    are the points b and c. To solve that, the user is expected to supply a
    function Deltafunc that returns the minimum distance between the points as
    a function of d0 with proper management if d0 is outside the range of lgrhox.

    The function returns
    xpars:  [a,b,c]
    ypars:  [eps_u, eps_u, eps_l]
    pint:   the found polynomial, callable
    """
    #extract positional arguments
    [lgrhox, eps_u, eps_l, d0] = args

    #extract keyword arguments
    #Deltafunc = kwargs['Deltafunc']
    verbose = kwargs['verbose']

    #Check if d0 is within the ranges of lgrhox
    #correct manually if needed
    if d0 < lgrhox[3]:
        print("Warning: d0 {:7.4f} < d0_min {:7.4f}".format(d0, float(lgrhox[3])))
        d0 = lgrhox[3]
    elif d0 > lgrhox[-4]:
        print("Warning: d0 {:7.4f} > d0_max {:7.4f}".format(d0, float(lgrhox[-4])))
        d0 = lgrhox[-4]

    #Conditions to satisfy
    #x0 < x1 < x2
    x0, y0 = (lgrhox[0]-0.1, eps_u)
    x1, y1 = (d0, eps_u)
    x2, y2 = (lgrhox[-1]+0.1, eps_l)

    xpars = [x0,x1,x2]
    ypars = [y0,y1,y2]

    if verbose:
        print("VProfileFunc: {0[0]:7.4f},{0[1]:7.4f},{0[2]:7.4f}".format(xpars))

    # the interpolating polynomial
    pint = interpolate.interp1d(xpars, ypars, kind='linear', assume_sorted=True)
    return xpars, ypars, pint

def vprofile_lip3p_v2(*args, **kwargs):
    """
    A function that finds the abundance as a function of lgrhox
    with linear piecewise polynomial from 3 points (second version).

    Input parameters:
        args = (lgrhox, eps_u, d0, T)
        kwargs = dict(nlayers=None, deltanlayers=None)

    lgrhox: log10 of the rhox array, gives the density
            as it changes with vertical depth
    eps:    abundance as a function of lgrhox

    eps_u:  abundance at the top layers of the atmosphere
    d0:     start of the slope
    T:      gradient of the slope

    Scheme of the vertical stratification profile:

      eps ^
          |
    eps_l x              -
          |             /|
          |            / |
          |           /  |
          |          /   |
          |         /    |
    eps_u x--------/     |
          |^       ^     |
          ||       |     |
          ||       |     |
          -x-------x-----x------------> lgrhox
           a       b     c

    This function computes the function values from the formula

           | eps_u, x <= d0
    f(x) = | or
           | eps_u + T * (x-d0), x > d0

    The function returns
    xpars:  [lgrhox[0], d0, lgrhox[-1]]
    ypars:  [eps_u, eps_u, eps_l]
    pint:   the found polynomial, callable
    """
    #extract positional arguments
    [lgrhox, eps_u, d0, T] = args

    #extract keyword arguments
    #Deltafunc = kwargs['Deltafunc']
    verbose = kwargs['verbose']

    #create the arrays for the polynomial
    x = np.asarray(lgrhox, dtype=np.float64)
    y = np.zeros(len(lgrhox), dtype=np.float64)

    y[:] += eps_u
    idx, = np.where(x > d0)
    y[idx] += T*(x[idx] - d0)

    #xpars = [x[0], d0, x[-1]]
    xpars = [eps_u, d0, T]
    ypars = [y[0], y[0], y[-1]]

    #print parameters eps_u, d0, T
    if verbose:
        print("VProfileFunc: {0[0]:7.4f},{0[1]:7.4f},{0[2]:7.4f}".format(xpars))

    # the interpolating polynomial
    pint = interpolate.interp1d(x, y, kind='linear', assume_sorted=True)
    return xpars, ypars, pint

def vprofile_lip5p(*args, **kwargs):
    """
    A function that finds the abundance as a function of lgrhox
    with linear piecewise interpolation from 5 points.

    Input parameters:
        args = (lgrhox, d1, T1, eps_m, d0, T0)
        kwargs = dict(nlayers=None, deltanlayers=None)

    lgrhox: log10 of the rhox array, gives the density
            as it changes with vertical depth
    eps:    abundance as a function of lgrhox

    T1:     abn. gradient of the top layers
    d1:     end of the gradient for the top layers
    eps_m:  abundance at the middle layers of the atmosphere (between d1 and d0)
    T0:     abn. gradient of the bottom layers
    d0:     end of the gradient for the bottom layers

    Scheme of the vertical stratification profile:

      eps ^          
          |          
    eps_u x    -        
          |    |\        
          |    | \        
          x    |  \                     -  eps_l
          |    |   \                   /|
          |    |    \                 / |
          |    |     \               /  |
          |    |      \             /   |
          |    |       \           /    |
    eps_m x    |        \---------/     |
          |    |         ^        ^     |
          |    |         |        |     |
          |    |         |        |     |
          -----x---------x--------x-----x------------> lgrhox
               a         b        c     d

    This function computes the function values from the formula

           | eps_m + T1 * (x-d1), x < d1
    f(x) = | eps_m, d1 <= x <= d0
           | eps_m + T0 * (x-d0), x > d0

    Warning: if d1 > d0 the results will be nonsensical.

    The function returns
    xpars:  [lgrhox[0], d1, d0, lgrhox[-1]]
    ypars:  [eps_u, eps_m, eps_m, eps_l]
    pint:   the found polynomial, callable
    """
    #extract positional arguments
    [lgrhox, d1, T1, eps_m, d0, T0] = args

    #extract keyword arguments
    #Deltafunc = kwargs['Deltafunc']
    verbose = kwargs['verbose']

    #create the arrays for the polynomial
    x = np.asarray(lgrhox, dtype=np.float64)
    y = np.zeros(len(lgrhox), dtype=np.float64)

    y[:] += eps_m
    idx, = np.where(x > d0)
    y[idx] += T0*(x[idx] - d0)
    idx, = np.where(x < d1)
    y[idx] += T1*(x[idx] - d1)

    # the interpolating polynomial
    pint = interpolate.interp1d(x, y, kind='linear', assume_sorted=True, bounds_error=False, fill_value=0)

    if d1 > d0:
        print("VProfileFunc: Warning d1 > d0! Nonsensical results. {:7.4f} > {:7.4f}".format(d1,d0))

    xpars = [x[0], d1, d0, x[-1]]
    ypars = pint(xpars)

    #print xpars
    if verbose:
        print("VProfileFunc: {0[0]:7.4f},{0[1]:7.4f},{0[2]:7.4f},{0[3]:7.4f}".format(xpars))

    return xpars, ypars, pint


class VProfileFunc(object):
    """
    Helper class for the vertical profile functions which 
    are defined above. It is used to facilitate homogeneous
    input and output of data for the vert. profile functions.
    
    Used by the VertStrat class.
    """
    def __init__(self, func, Deltafunc=None, verbose=True):
        """
        Pass vprofile1p or vprofile4p at init. The next time
        the instance gets called it will use self.func to
        calculate the vert. profile function.
        """
        self.func = func
        self.Deltafunc = Deltafunc
        self.verbose = verbose

    def __call__(self, *args):
        """
        Pass (lgrhox, eps_u, eps_l, d0, Delta) as arguments
        to calculate the vert. profile function

        TODO: Add check so that we don't redo everything
        if the arguments match everything from last time
        """
        kwargs = dict(Deltafunc=self.Deltafunc,
                      verbose=self.verbose)
        xpars, ypars, pint = self.func(*args, **kwargs)
        self.xpars = xpars
        self.ypars = ypars
        self.pint = pint
        return pint


class VertStrat(object):
    """
    This is the class that facilitates the computation of a given number
    of small spectral regions that are defined with
        - zdata: a dictionary of Zeeman line lists, 
        - obsdata: a list of dicts that define the obs. data,
        - krz: model atmosphere in Kurucs format.
    
    Note that the model atmosphere is same for all spectral regions.
    """
    def __init__(self, zdata, obsdata, krz, vsini=None, vmacro=None):
        """
        An example zdata dictionary (variable zdata):

        from magfield import pyvald, pykrz

        zlist1 = pyvald.read_zfile('zlin/Ca1_4227.z')
        zlist2 = pyvald.read_zfile('zlin/Ca1_4512.z')
        zlist3 = pyvald.read_zfile('zlin/Ca2_5021.z')
        zdata = {1:zlist1, 2:zlist2, 3:zlist3}


        An example obsdata dictionary (variable obsdata):

        obs1 = {'zkey':1, 'obs':(wl, sp), 'wstart':4226.25, 'wend':4227.10,
                'wshift': 0.003, 'resol':110000., 'weights':1.0}
        obs2 = {'zkey':2, 'obs':(wl, sp), 'wstart':4512.17, 'wend':4512.39,
                'wshift':-0.000, 'resol':110000., 'weights':1.0}
        obs3 = {'zkey':3, 'obs':(wl, sp), 'wstart':5021.00, 'wend':5021.30,
                'wshift':-0.000, 'resol':110000., 'weights':1.0}
        obsdata = [obs1, obs2, obs3]


        An example atmosphere model input (variable krz):

        krz = pykrz.read_krzfile('hd24712_homog.krz')

        vsini and vmacro are the the projected rotational velocity and
        radial-tangential macroturbulence.

        At the moment of instantion of the class we copy static
        data kept in zdata and krz, and cut out the necessary parts of
        the observed spectra given in obsdata.
        """
        #the atmosphere model (output from read_krz)
        self.krz = krz
        self.krzfile = 'input.krz'

        # prepare the synmast input files
        self.zdata = zdata
        self.zprint = {}
        self.zkeys = []
        for zkey, zlist in zdata.viewitems():
            zlist['krz'] = self.krzfile
            zprint = pyvald.print_zlist(zlist)
            self.zprint[zkey] = zprint
            self.zkeys.append(zkey)

        # go over the observations
        self.obskeys = [] #keys of zdata files required by the observations
        self.wstart = []
        self.wend = []
        self.wshift = []
        self.resol = []
        self.weights = []
        self.obs = dict(wl=[], sp=[]) # Dictionary for the observed spectra
        self.syn = dict(wl=[], sp=[]) # Dictionary for the synthetic spectra
        for obs in obsdata:
            self.obskeys.append(obs['zkey'])
            self.wstart.append(obs['wstart'])
            self.wend.append(obs['wend'])
            self.wshift.append(obs['wshift'])
            self.resol.append(obs['resol'])
            self.weights.append(obs['weights'])

            #cut only what we need from the observations
            idx = cut_wl(obs['obs'][0], obs['wstart'], obs['wend'])
            self.obs['wl'].append(obs['obs'][0][idx])
            self.obs['sp'].append(obs['obs'][1][idx])

            #just fill them out with None values
            self.syn['wl'].append(None)
            self.syn['sp'].append(None)

        #parameters for the integration procedure
        if vsini is not None:
            self.vsini = float(vsini)
        else:
            self.vsini = 0.0

        if vmacro is not None:
            self.vmacro = float(vmacro)
        else:
            self.vmacro = 0.0

        #this needs to be initiated in self.setup()
        self.setup_done = False

        self.chisq_value = None #chisquare function
        self.chisq_gradient_array = None #gradient of the chisquare function

    def setup(self, el, params0, fixparams=None, dparams=None,
              bounds=None, nproc=None, woffset=0.5, tsleep=0.1,
              model='hpp', oversample=5, nlayers=10, verbose=False, debug=False):
        """
        This subroutine sets up some internal parameters that are necessary
        for the calculations of the spectral regions. In theory we could have
        done this in the __init__ subroutine, but the code is cleaner like this.

        Input parameters:
            el: Name of the chemical element we try to fit

            params0: [eps_u, eps_l, d0, Delta] starting guess for
                the parameters of the vertical profile function.

            fixparams: Which parameters should be fixed. The default
                is to fit all parameters of the model.  In the general case 1
                is fixed, 0 is free.

            dparams: Step for the finite differences procedure.

            bounds: Boundaries for the parameters. If set to None
                the code tries to automatically find the min and max.
                values for each variable in params0.

            nproc: Number of processes to use. The default is
                None, i.e., we get nproc from multiprocessing.cpu_count()

            woffset: Not used.

            tsleep: Time interval for checking if the process
                has finished with the calculations, default=0.1 sec.

            nlayers: The minimal number of atmospheric layers for the
                transiton window. This is a technical parameter in order to
                avoid unexpected jumps in the atmosphere model.

            deltanlayers: This is passed to an instance of VProfileFunc. Check
                that one for an explanation.

            oversample: Overample the model atmosphere on a finer grid in
                lgrhox units using Akima interpolation splines. The new points
                are spread out equidistantly between the original points. The
                default is to oversample the original model atmosphere 5 times.

            model: Selects the type model used to represent the vertical
                stratification gradient. The default is Hermitian piecewise
                polynomial 'hpp', other choices are linear interpolating poly-
                nomial 'lip' and one parameter function for fitting homoge-
                nenous abundace 'homog'.

            debug: Gets passed to the vprofile functions as a verbose argument.
        """
        #chemical element to fit
        self.zelem = pykrz.get_zelem(el)
        self.aelem = pykrz.get_aelem(self.zelem)

        #process the initial guess
        self.params0 = np.asarray(params0, dtype=np.float64)
        self.nparams = len(self.params0)

        #used for calculating the gradient (if needed)
        if dparams is None:
            self.dparams = 0.05 * np.ones(self.nparams)
        else:
            self.dparams = np.asarray(dparams, dtype=np.float64)
        self.ndparams = len(self.dparams)
        assert self.ndparams == self.nparams

        #set the fixed and free parameters
        if fixparams is None:
            self.fixparams = np.zeros(self.nparams, dtype=np.int32)
        else:
            self.fixparams = np.asarray(fixparams, dtype=np.int32)
        assert len(self.fixparams) == self.nparams

        #indices of the free and fixed params (for fast access)
        self.ifree = np.where(self.fixparams == 0)[0]
        self.ifixed = np.where(self.fixparams == 1)[0]

        #this is done so that we don't recalculate the function
        #if the input parameters are the same
        self.old_params = np.ones(self.nparams, dtype=np.float64) * 1e100

        #set the model of the vertical stratification gradient
        self.model = model.lower().strip()
        if self.model == 'hpp':
            self.vprofilefunc = VProfileFunc(vprofile_hpp, verbose=debug)
            assert self.nparams == 4
        elif self.model == 'lip':
            self.vprofilefunc = VProfileFunc(vprofile_lip, verbose=debug)
            assert self.nparams == 4
        elif self.model == 'homog':
            self.vprofilefunc = VProfileFunc(vprofile_homog, verbose=debug)
            assert self.nparams == 1
        elif self.model == 'lip3p':
            self.vprofilefunc = VProfileFunc(vprofile_lip3p, verbose=debug)
            assert self.nparams == 3
        elif self.model == 'lip3p_v2':
            self.vprofilefunc = VProfileFunc(vprofile_lip3p_v2, verbose=debug)
            assert self.nparams == 3
        elif self.model == 'lip5p':
            self.vprofilefunc = VProfileFunc(vprofile_lip5p, verbose=debug)
            assert self.nparams == 5
        else:
            raise ValueError('The model input parameter not understood.')

        #prepare the atmosphere model
        self.oversample = oversample
        self.oversample_krz()

        #check boundaries (update if necessary), requires krz atm. model
        self.check_bounds(bounds)

        #calculate the minimal width of Delta as a function of lgrhox
        #this is somewhat arbitrary and not applicable to every model
        width = self.lgrhox[1:] - self.lgrhox[:-1]
        pos = self.lgrhox[:-1] + width/2
        self.vprofilefunc.Deltafunc = interpolate.interp1d(pos, width,
            bounds_error=False, fill_value=np.max(width))
        #self.vprofilefunc.Deltafunc = interpolate.pchip(pos, width,
        #    extrapolate=True)
        self.nlayers = nlayers

        #prepare the gradient dictionary
        self.grad = dict()
        for i in self.ifree:
            syn = dict()
            syn['wl'] = [None for obskey in self.obskeys]
            syn['sp'] = [None for obskey in self.obskeys]
            self.grad[i] = syn

        #max. number of processes
        if nproc is None:
            self.nproc = multiprocessing.cpu_count()
        else:
            self.nproc = int(nproc)

        #Initiate the workers
        self.workers = [Worker(krzfile=self.krzfile) for i in range(self.nproc)]

        #used by self.process_result() when convolving the spectrum with a gaussian
        self.woffset = float(woffset)

        #this gets checked by the fitting procedure
        self.setup_done = True

        #checks the workers every tsleep seconds for free worker
        self.tsleep = float(tsleep)

        #print more info (Progress bar and Time)
        self.verbose = verbose

    def check_bounds(self, bounds):
        """
        This is called by self.setup() to automatically determine
        the bounds for the parameters.

        If some bounds are None here, they are computed automatically.

        Requires self.krz to be present. Not used currently.
        """
        if bounds is None:
            bounds = [None]*self.nparams

        # automatic boundary setup
        lgrhox_min = np.log10(self.krz['rhox'][0])
        lgrhox_max = np.log10(self.krz['rhox'][-1])

        # automatic abundance boundaries
        eps_min = -20
        eps_max = -1

        # Everything below is model dependent
        if self.model == 'lip' or self.model == 'hpp':
            if bounds[0] is None: #eps_u
                bounds[0] = (eps_min, eps_max)

            if bounds[1] is None: #eps_l
                bounds[1] = (eps_min, eps_max)

            if bounds[2] is None: #d0
                bounds[2] = (lgrhox_min, lgrhox_max)

            if bounds[3] is None: #Delta
                bounds[3] = (0.01, (lgrhox_max - lgrhox_min)*2)
        elif self.model == 'lip3p':
            if bounds[0] is None: #eps_u
                bounds[0] = (eps_min, eps_max)

            if bounds[1] is None: #eps_l
                bounds[1] = (eps_min, eps_max)

            if bounds[2] is None: #d0
                bounds[2] = (lgrhox_min, lgrhox_max)
        elif self.model == 'homog':
            if bounds[0] is None:
                bounds[0] = (eps_min, eps_max)
        elif self.model == 'lip3p_v2':
            if bounds[0] is None: #eps_u
                bounds[0] = (eps_min, eps_max)

            if bounds[1] is None: #d0
                bounds[1] = (lgrhox_min, lgrhox_max)

            if bounds[2] is None: #T, I don't know the reasonable boundaries for the gradient
                bounds[2] = (-10, 10)
        self.bounds = bounds

    def stop(self):
        """Calls the delete method for the instantiated workers."""
        if not self.setup_done: return

        for worker in self.workers:
            worker.delete()

    def funcgradient(self, params, gradient=True):
        """
        This computes the synthetic spectra and the gradient if that is
        required, and additionally it calculates the chi square value. For
        the latter we can also use the canned method self.chisq_function()

        Input:
            params: [eps_u, eps_l, d0, Delta] calculates the synthetic
                profiles for these parameters.

            gradient: set it to True if you want the code to compute the
                gradient at each point.
        
        
        How it works:
          1. Pre-compute all necessary data for each spectral region.
          2. Package the input data as a generator that is fed to the workers.
          3. Load up the workers with the initial work data.
          4. A Queue-like System monitors the workers and feeds them new data.
          5. At the end, calculate the chisq vector and print some useful data.
          
        Note: processing of the data from workers is done by self.process_result.
        """
        self.params = np.copy(self.params0) #copy the initial guesss to params
        self.params[self.ifree] = params  # now we have the full vector with
                                          # the free and fixed parameters.

        #keeps input data for the workers
        args = []

        #construct the initial interpolating polynomial
        #do it only if for the previous step we had different params vector
        if not np.allclose(self.old_params, self.params):
            pint = self.vprofilefunc(self.lgrhox, *self.params)
            krz, lgrhox = self.update_krz()

            #update the krz data with the new stratification profile
            krz['strat'][self.zelem] = pint(lgrhox)
            krz['nstrat'] = len(krz['strat'])
            args.append((-1, pykrz.print_krz(krz)))

        #now go through the free parameters and calculate the stratification profiles
        #that we use to compute the gradient using forward differences
        if gradient:
            krz, lgrhox = self.update_krz()
            for ikey in self.ifree:
                params = np.copy(self.params)
                params[ikey] += self.dparams[ikey]
                pint = self.vprofilefunc(self.lgrhox, *params)

                #update krz data with the new stratification profile
                krz['strat'][self.zelem] = pint(lgrhox)
                krz['nstrat'] = len(krz['strat'])
                args.append((ikey, pykrz.print_krz(krz)))

        #create the arguments for the worker
        zkeys = np.unique(self.obskeys) #relevant zkeys needed by the observations
        zlists = [(zkey,self.zprint[zkey]) for zkey in zkeys]
        tasks = itertools.product(args, zlists) #this is efficient. It doesn't exist whole in memory.
        ntasks = len(zlists) * len(args) #number of all tasks that are going to be run

        #nothing to do
        if ntasks == 0: return

        #start timer
        self.time_start = time.time()

        #load up the initial tasks
        iproc = 0; itask = 0
        self.used = [False for i in range(self.nproc)] #all workers are unused at the start
        while iproc < self.nproc and itask < ntasks:
            (ikey, krzprint), (zkey, zprint) = tasks.next()
            info = dict(ikey=ikey, zkey=zkey)
            self.workers[iproc].synmast(zprint, krzprint, info=info)
            self.used[iproc] = True
            iproc += 1; itask += 1
            #print itask, ntasks

        #feed the workers new tasks as long as there is something in the pipeline
        itask = 0
        for task in tasks:
            #wait until any worker finishes
            cycle = True
            while cycle:
                for worker in self.workers:
                    if worker.done():
                        self.process_result(worker)
                        cycle = False #breaks the while loop
                        break #exits the for loop
                else:
                    #pause for a while before rechecking again
                    time.sleep(self.tsleep)

            #load it up with new data and restart it again
            (ikey, krzprint), (zkey, zprint) = task
            info = dict(ikey=ikey, zkey=zkey)
            worker.synmast(zprint, krzprint, info=info)
            itask += 1
            #print itask, ntasks


        #wait for the remaining tasks to finish and gather the results
        while itask < ntasks:
            cycle = True
            while cycle:
                for i in range(self.nproc):
                    if self.used[i]:
                        if self.workers[i].done():
                            self.process_result(self.workers[i])
                            self.used[i] = False
                            cycle = False
                            break
                else:
                    time.sleep(self.tsleep)
            itask += 1
            #print itask, ntasks

        # copy the current params vector for next time
        # if this function gets called with the gradient flag,
        # but not with the function flag for the same parameters
        # avoid doing the same initial calculations and go right to
        # the gradient calculations
        self.old_params = np.copy(self.params)

        #recalculate the chisq value
        chisq_value = 0.0
        for i in range(len(self.obskeys)):
            chisq_value += np.sum((self.obs['sp'][i]-self.syn['sp'][i])**2 *self.weights[i])
        self.chisq_value = chisq_value

        # print some useful data
        if self.verbose:
            #calculate the time it took to do the computations
            self.time_end = time.time()
            m, s = divmod(self.time_end - self.time_start, 60)
            h, m = divmod(m, 60)

            #tell what we were calculating here (function or gradient)
            if gradient:
                stype = 'grad'
            else:
                stype = 'func'

            #print additional information
            spars = self.string_params(self.params)
            stime = "{0:02.0f}:{1:02.0f}:{2:04.1f}".format(h,m,s)
            print('funcgradient: dTime={} chisq={:12.7e} Type={} x={}'.format(stime,self.chisq_value,stype,spars))

    def string_params(self, x):
        """Returns a formatted string with the parameter values in x"""
        if self.model == 'homog':
            spars = "({0[0]:08.4f})".format(x)
        elif self.model == 'lip3p':
            spars = "({0[0]:08.4f}, {0[1]:08.4f}, {0[2]:05.3f})".format(x)
        elif self.model == 'lip3p_v2':
            spars = "({0[0]:08.4f}, {0[1]:06.2f}, {0[2]:06.2f})".format(x)
        elif self.model == 'lip5p':
            spars = "({0[0]:06.2f}, {0[1]:06.2f}, {0[2]:08.4f}, {0[3]:06.2f}, {0[4]:06.2f})".format(x)
        else:
            spars = "({0[0]:08.4f}, {0[1]:08.4f}, {0[2]:05.3f}, {0[3]:05.3f})".format(x)
        return spars

    def process_result(self, worker):
        """
        The input is an instance of the Worker class.

        The method is called by self.funcgradient each time a worker
        has finished running the synmast code.

        Disk integrate and convolve with vsini, vmacro and the given
        spectral resolution for each wavelength range, given by
        self.obskeys, that depends on this synthetic spectrum. This is
        done by the worker.s3div method.

        Once that is done, read the data and interpolate them on the
        wavelength grid of the observations.

        The results for each key in self.obskeys are saved with the
        same keys in self.syn and self.grad
        """
        # go through the wavelength regions
        idx, = np.where(worker.info['zkey'] == np.asarray(self.obskeys))
        #print worker.info['zkey'], self.obskeys
        for i in idx:
            #disk integration and convolution
            worker.s3div(vsini=self.vsini, vmacro=self.vmacro, resol=self.resol[i])

            # apply wavelength shift
            wl = worker.wl + self.wshift[i]

            # cut the piece that we need
            #jdx = cut_wl(wl, self.wstart[i] - self.woffset, self.wend[i] + self.woffset)
            #wl = wl[jdx]
            #sp = result['sp'][jdx]

            # Apply Gaussian instrumental broadening
            #sp = pyasl.instrBroadGaussFast(wl, sp, self.resol[i],
            #    edgeHandling="firstlast", fullout=False)

            # interpolate the synthetic spectrum on the grid
            # of the observations
            sp = np.interp(self.obs['wl'][i], wl, worker.sp)

            #print result['ikey'], result['zkey'], self.wstart[i], self.wend[i]

            # copy the results where they belong
            if worker.info['ikey'] < 0:
                self.syn['sp'][i] = sp #for other purposes
            else:
                self.grad[worker.info['ikey']]['sp'][i] = sp #for gradient calculations

    def chisq_function(self, params):
        """
        Calculate the chi square value given by

            sum((obs - syn)**2 * weights),

        for each wavelength region, specified in self.obskeys.

        This can be ran after self.setup() has been called.
        """
        # return previous chisq value (if it exists)
        if np.allclose(self.old_params, params) and self.chisq_value is not None:
            return self.chisq_value

        #recalculate the synthetic profiles and the chisq value
        self.funcgradient(params, gradient=False)
        return self.chisq_value

    def chisq_gradient(self, params):
        """
        Calculate the gradient of the chi square function given by

            sum((obs - syn)**2 * weights),

        for each wavelength region, specified in self.obskeys, with
        respect to the free parameters given in self.ifree.

        At input we pass params=[eps_u, eps_l, d0, Delta].

        This can be ran after self.setup() has been called.
        """
        # Calculate the synthetic profiles for the given params
        self.funcgradient(params, gradient=True)

        #calculate the difference (obs - syn) * weight
        diff = []
        for i in range(len(self.obskeys)):
            diff.append((self.obs['sp'][i] - self.syn['sp'][i]) * self.weights[i])

        #calculate the gradient only for the free parameters
        chisq_gradient_array = []
        for ikey in self.ifree:
            val = 0.0
            for j in range(len(self.obskeys)):
                partial = (self.grad[ikey]['sp'][j] - self.syn['sp'][j]) / self.dparams[ikey]
                val += -2 * np.sum(partial * diff[j])
            chisq_gradient_array.append(val)
        self.chisq_gradient_array = chisq_gradient_array
        return chisq_gradient_array

    def leastsq_function(self, params):
        """
        Returns the value of the function

            (obs_i - syn_i) * weights_i

        for all wavelength points.

        This is used by the self.leastsq_fit method.
        """
        # don't recalculate the synthetic spectra if they already exist
        old_params_free = np.copy(self.old_params)[self.ifree]
        if not np.allclose(old_params_free, params):
            self.funcgradient(params, gradient=False)

        #calculate the difference (obs - syn)*weight
        diff = []
        for i in range(len(self.obskeys)):
            arr = (self.obs['sp'][i] - self.syn['sp'][i]) * self.weights[i]
            diff += arr.tolist()
        return np.asarray(diff)

    def leastsq_jacobian(self, params):
        """
        Returns the Jacobian of the function

            (obs_i - syn_i) * weights_i

        for all wavelength points with respect to
        the free parameters as given by self.ifree.

        This is used by the self.leastsq_fit method.
        """
        # Will not recalculate the synthetic profiles
        # if there were already calculated
        self.funcgradient(params, gradient=True)

        jacobian = []
        for ikey in self.ifree:
            partial = []
            for j in range(len(self.obskeys)):
                arr = -self.weights[j] * (self.grad[ikey]['sp'][j] - self.syn['sp'][j]) / self.dparams[ikey]
                partial += arr.tolist()
            jacobian.append(partial)
        return np.asarray(jacobian).transpose()

    def leastsq_romberg(self, params):
        """
        Experimental.
        
        Uses the Romberg differential scheme to
        more accurately estimate the matrix jacobian, 
        however it takes a very long time to do that.
        """
        jac = {'d':None, 'd/2':None, 'd/4':None} #container for the results
        dparams = np.copy(self.dparams) #There are the original values

        #run 1
        jac['d'] = self.leastsq_jacobian(params)

        #run 2
        self.dparams /= 2.
        jac['d/2'] = self.leastsq_jacobian(params)

        #run 3
        self.dparams /= 2.
        jac['d/4'] = self.leastsq_jacobian(params)

        #restore original values
        self.dparams = np.copy(dparams)

        return jac['d']/3 - 2*jac['d/2'] + 8*jac['d/4']/3

    def leastsq_fit(self, **kwargs):
        """
        Performs non-linear least squares fit for the parameters
        that define how the abundance of a given element changes
        with depth in the atmosphere of a star. 
        
        
        This method calls the following scipy function:
        
        optimize.leastsq(func, x0, args=(), Dfun=None,
            full_output=0, col_deriv=0, ftol=1.49012e-08, xtol=1.49012e-08,
            gtol=0.0, maxfev=0, epsfcn=None, factor=100, diag=None)

        Passes arguments to the scipy minimization function through **kwargs.
        """
        # initial guess for the free parameters
        x0 = np.copy(self.params0)[self.ifree]

        options = dict(full_output=1,
                       Dfun=self.leastsq_jacobian)
        options.update(**kwargs)
        #options['Dfun'] = self.leastsq_romberg
        return optimize.leastsq(self.leastsq_function, x0, **options)

    def simplex_fit(self, **kwargs):
        """
        Uses the simplex method, which doesn't use gradient information, 
        to find the minimum chisq value and thus estimate the 
        optimal parameters that solve the problem. 
        
        
        This method calls the following scipy function:
        
        optimize.fmin(func, x0, args=(),
            xtol=0.0001, ftol=0.0001, maxiter=None, maxfun=None,
            full_output=0, disp=1, retall=0, callback=None)

        Passes arguments to the scipy minimization function through **kwargs.
        """
        # initial guess for the free parameters
        x0 = np.copy(self.params0)[self.ifree]

        options = dict(xtol=0.01, #equals to one percent change
                       ftol=0.01, #equals to one percent change
                       full_output=1,
                       disp=5)
        options.update(**kwargs)
        return optimize.fmin(self.chisq_function, x0, **options)

    def tnc_fit(self, **kwargs):
        """
        Uses the truncated Newton algorithm to minimize a function
        with variables that can be bounded. It uses a gradient 
        information to better estimate each step.
        
        This method calls the following scipy function:
        
        optimize.fmin_tnc(func, x0, fprime=None, args=(),
            approx_grad=0, bounds=None, epsilon=1e-08, scale=None,
            offset=None, messages=15, maxCGit=-1, maxfun=None, eta=-1,
            stepmx=0, accuracy=0, fmin=0, ftol=-1, xtol=-1, pgtol=-1,
            rescale=-1, disp=None, callback=None)

        Passes arguments to the scipy minimization function through **kwargs.
        """
        # initial guess for the free parameters
        # copy the boundaries as well
        x0 = np.copy(self.params0)[self.ifree]
        bounds = []
        for ikey in self.ifree:
            bounds.append(self.bounds[ikey])

        #prepare the input dictionary for the minimization function
        options = dict(bounds=bounds,
                       disp=5,
                       fprime=self.chisq_gradient)
        options.update(kwargs)
        return optimize.fmin_tnc(self.chisq_function, x0, **options)

    def diff_evolution(self, **kwargs):
        """
        Experimental.
        
        Uses differential evolution to find the solution that gives
        the best minimizes the chisq function. 
        
        This method calls the following scipy function:
        
        optimize.differential_evolution(func, bounds, args=(),
            strategy='best1bin', maxiter=None, popsize=15, tol=0.01,
            mutation=(0.5, 1), recombination=0.7, seed=None,
            callback=None, disp=False, polish=True,
            init='latinhypercube')

        Passes arguments to the scipy minimization function through **kwargs.
        """
        seed = 152
        bounds = []
        for ikey in self.ifree:
            bounds.append(self.bounds[ikey])

        #prepare the input dictionary for the minimization function
        options = dict(seed=seed,
                       disp=True)
        options.update(kwargs)
        func = self.chisq_function
        return optimize.differential_evolution(func, bounds, **options)

    def fit(self, method, **kwargs):
        """
        Start the fitting procedure.
        
        The method used for the minimization of the chisq function can be:
        
        - 'leastsq' (Levenberg-Marquardt minimization, calculates gradients)
        - 'simplex' (Simplex method minimization, only funcation evaluations)
        - 'tnc' (Truncated Newton method with bounds for the parameters)
        
        Passes arguments to the scipy minimization function through **kwargs.
        """
        # Check that we have correct number of free parameters
        if self.ifree.size == 0 or self.ifree.size > self.nparams:
            raise ValueError("Number of free parameters={} doesn\'t match number \
                  of total parameters={}.".format(self.ifree.size,self.nparams))

        if method.lower().strip() == 'leastsq':
            return self.leastsq_fit(**kwargs)
        elif method.lower().strip() == 'simplex':
            return self.simplex_fit(**kwargs)
        elif method.lower().strip() == 'tnc':
            return self.tnc_fit(**kwargs)
        else:
            raise ValueError("Method %s not recognised." %method)

    def errorbarsfunc(self, x, ikey, params0):
        """Helper function used by the method self.asymmetric_errorbars"""
        params = np.copy(params0)
        params[ikey] = x #update just this value

        #main calculations block
        #this might change in the future
        return self.chisq_function(params)

    def asymmetric_errorbars(self, params0, yopt, fixparams=None, bounds=None,
                  xtol=None, atol=1e-3, rtol=1e-2, nmax=10, verbose=True):
        """
        Input variables:
            params0: best fit for [eps_u, eps_l, d0, Delta]

            yopt: value of chisq that we need to reach

            fixparams: if this is None, the method proceeds to find the
                assymetric error bars for the parameters given by self.fixparams.

            bounds: if None use the boundaries as defined by the self.setup method

            xtol: stop iterating when the distance between two consecutive
                values of x becomes less than xtol.

            atol: absolute tolerance for the function values

            rtol: relative tolerance for the function values

            nmax: maximum number of iterations.

        This method calls the bracket function to determine the upper and lower
        error bars for the parameters at question separately.

        The algorithm stops the iterations when one of the following conditions
        are not True any more:
            condition 1: |x1 - x2| < xtol
                the distance between two consecutive values for x
                becomes less than xtol.

            condition 2: |y1 - y2| < ftol = atol + rtol * |y2|
                the distance between two consecutive values of the function
                (y-axis) becomes less than ftol.

            condition 3: niter < nmax
                Maximum number of iterations has been reached.

            condition 4: for all x in [xmin, xmax], y=func(x) < yopt
                For all values of x in [xmin, xmax], given by the variable bounds,
                the function values are less than the requested value yopt.

        The method returns a dictionary with keys that match the indices of the
        parameters that were set free in fixparams.
        """
        if fixparams is None:
            fixparams = self.fixparams
            ifree = self.ifree
        else:
            fixparams = np.asarray(fixparams)
            ifree = np.where(fixparams == 0)[0]

        if bounds is None:
            bounds = self.bounds
        else:
            bounds = np.asarray(bounds)

        assert len(bounds) == len(params0) == len(fixparams)

        results = {}
        y1 = self.chisq_function(params0)
        params_lbls = ['eps_u', 'eps_l', 'd0', 'Delta']
        for ikey in ifree:
            args = (ikey, np.copy(params0)) #additional arguments to self.errorbarsfunc

            if bounds[ikey] is None:
                if verbose:
                    print("---Bounds for ikey=%d are None. Skipping this one." %ikey)
                results[ikey] = [(None,None), (None,None)]
                continue

            if verbose:
                print("---Lower boundary for %s" %params_lbls[ikey])
            #lower boundary (towards the minimum)
            step = -self.dparams[ikey]
            x1 = params0[ikey]
            xbounds = bounds[ikey]
            xmin, ymin = bracket(self.errorbarsfunc, y1, yopt, x1, xbounds, step,
                                 args, nmax, xtol, atol, rtol, verbose)

            #upper boundary
            if verbose:
                print("---Upper boundary for %s" %params_lbls[ikey])
            step = self.dparams[ikey]
            x1 = params0[ikey]
            xbounds = bounds[ikey]
            xmax, ymax = bracket(self.errorbarsfunc, y1, yopt, x1, xbounds, step,
                                 args, nmax, xtol, atol, rtol, verbose)

            #save results
            results[ikey] = [(xmin,ymin), (xmax,ymax)]
        return results

    def oversample_krz(self):
        """
        Oversample the model atmosphere on a denser grid.

        The oversampling is performed on a log10 grid of rhox using
        Akima interpolating cubic splines, which preserves monotonicity
        of the original data and produces natural looking curves.

        The new points are added on equal intervals between original points.
        The number of new subintervals is defined by self.oversample

        If oversample is equal to one that means that we don't need to
        oversample the model atmosphere on a denser grid.
        """
        self.lgrhox = np.log10(self.krz['rhox'])
        #add new layers
        if self.oversample > 1:
            lgrhox = []
            subgrid = np.arange(self.oversample)
            for i in range(1,len(self.lgrhox),1):
                dx = self.lgrhox[i] - self.lgrhox[i-1]
                lgrhox += list(subgrid*dx/self.oversample + self.lgrhox[i-1])
            lgrhox += [self.lgrhox[-1]] # the last point has to be added manually
            lgrhox = np.asarray(lgrhox)
        else:
            lgrhox = None

        #create the interpolating splines
        self.krz_finterp = {}
        self.krz_finterp['t'] = interpolate.Akima1DInterpolator(self.lgrhox,
            np.asarray(self.krz['t']))
        self.krz_finterp['xne'] = interpolate.Akima1DInterpolator(self.lgrhox,
            np.asarray(self.krz['xne']))
        self.krz_finterp['xna'] = interpolate.Akima1DInterpolator(self.lgrhox,
            np.asarray(self.krz['xna']))
        self.krz_finterp['rho'] = interpolate.Akima1DInterpolator(self.lgrhox,
            np.asarray(self.krz['rho']))

        #in case we have stratification profiles for other elements we also need
        #to prepare interpolation splines for them
        self.krz_finterp['strat'] = {}
        if self.krz['nstrat'] > 0:
            for key in self.krz['strat'].keys():
                abund = np.asarray(self.krz['strat'][key])
                finterp = interpolate.Akima1DInterpolator(self.lgrhox, abund)
                self.krz_finterp['strat'][key] = finterp

        #interpolate if necessary
        if lgrhox is not None:
            self.krz['rhox'] = 10**lgrhox
            self.krz['t'] = self.krz_finterp['t'](lgrhox)
            self.krz['xne'] = self.krz_finterp['xne'](lgrhox)
            self.krz['xna'] = self.krz_finterp['xna'](lgrhox)
            self.krz['rho'] = self.krz_finterp['rho'](lgrhox)
            self.krz['nrhox'] = lgrhox.size
            self.lgrhox = lgrhox

            #interpolate the other stratification profiles (if present)
            if self.krz['nstrat'] > 0:
                for key in self.krz['strat'].keys():
                    self.krz['strat'][key] = self.krz_finterp['strat'][key](lgrhox)

    def update_krz(self):
        #update_krz is not necessary for a homogeneous or three points model
        if self.model == 'homog' or self.model == 'lip3p':
            lgrhox = np.copy(self.lgrhox)
            krz = copy.deepcopy(self.krz)
            return krz, lgrhox

        #the start and end of the transition window
        x1, x2 = self.vprofilefunc.xpars[1:3]
        lgrhox = np.copy(self.lgrhox)

        #Make sure that there are at least self.nlayers points between x1 and x2
        #This will fail if there are no points between x1 and x2
        #However, that should be prevented by vprofilefunc and the initial
        #oversampling of the atmosphere model
        #idx1 = np.where(lgrhox < x1)[0][-1] #the largest lgrhox that is smaller than x1
        #idx2 = np.where(lgrhox > x2)[0][0] #the smallest lgrhox that is larger than x2
        #nlayers = idx2 - idx1 - 1 #current number of intervals between the x1 and x2

        idx, = np.where((lgrhox >= x1) & (lgrhox <= x2))
        nlayers = len(idx) - 1

        if nlayers  < self.nlayers:
            #oversample the entire grid
            lgrhox_ = []
            dx = lgrhox[1:] - lgrhox[:-1]
            for i in range(0,len(lgrhox)-1,1):
                lgrhox_ += [lgrhox[i], lgrhox[i] + dx[i]/2]
            lgrhox_.append(lgrhox[-1])
            lgrhox_ = np.asarray(lgrhox_)

            #take from it only the part that's between x1 and x2
            lgrhox_new = []

            idx, = np.where(lgrhox < x1)
            lgrhox_new += lgrhox[idx].tolist()

            idx, = np.where((lgrhox_ >= x1) & (lgrhox_ <= x2))
            lgrhox_new += lgrhox_[idx].tolist()

            idx, = np.where(lgrhox > x2)
            lgrhox_new += lgrhox[idx].tolist()
            
            lgrhox = np.asarray(lgrhox_new)

            #recheck the number of points between x1 and x2
            idx, = np.where((lgrhox >= x1) & (lgrhox <= x2))
            nlayers = len(idx) - 1

        #the new and updated model atmosphere
        krz = copy.deepcopy(self.krz)

        #interpolate on the new grid
        #this won't work if we have stratification profiles for other elements
        if lgrhox.size > self.lgrhox.size:
            krz['rhox'] = 10**lgrhox
            krz['t'] = self.krz_finterp['t'](lgrhox)
            krz['xne'] = self.krz_finterp['xne'](lgrhox)
            krz['xna'] = self.krz_finterp['xna'](lgrhox)
            krz['rho'] = self.krz_finterp['rho'](lgrhox)
            krz['nrhox'] = lgrhox.size

            #interpolate the other stratification profiles (if present)
            if krz['nstrat'] > 0:
                for key in krz['strat'].keys():
                    krz['strat'][key] = self.krz_finterp['strat'][key](lgrhox)

            if self.verbose:
                print("Warning: {} points added between {:7.4f} and {:7.4f}.".
                      format(lgrhox.size - self.lgrhox.size, x1, x2))

        return krz, lgrhox

class VertStrat3D(object):
    """
    This class tries to solve the problem of Vertical Stratification for a given
    chemical element for all phases for which we have observational data. It also
    introduces time-circular regularization for the solution so that the parameters
    don't change chaotically from one phase to the next. It builds upon VertClass.
    """
    def __init__(self, mfield, obsdata, zdata, krz, vsini=None, vmacro=None):
        """
        Instantiates the class and copies the supplied data to each VertStrat worker.
        
        Input variables:
          - mfield, a list with the field configuration (Br, Bm, Ba) for each phase,
          - obsdata, a list of dictionaries that contain the observational data and
            other parameters relating to the observations for each rotational phase.
          - zdata, line lists for each spectral region. This is one dictionary for all
            phases.The only value that gets changed for every phase is the magnetic
            field configuration.
          - krz, the atmosphere model in Kurucs format.
          - vsini, rpjected equatorial velocity (km/s)
          - vmacro, macroturbulent velocity (km/s)
        """
        self.vstrat = [] # here we keep the vstrat instances for individual phases
        self.nphases = len(mfield)

        assert len(mfield) == len(obsdata)

        for i in range(len(mfield)):
            #update the zdata with the magnetic field configuration for the curent phase
            newzdata = {}
            for key in zdata.keys():
                zdata[key]['header']['mfield'] = mfield[i]
                newzdata[key] = copy.deepcopy(zdata[key])

            #copy the atmosphere model
            newkrz = copy.deepcopy(krz)

            #the new instance of VertStrat for the current phase
            newvstrat = VertStrat(newzdata, obsdata[i], newkrz, vsini=vsini, vmacro=vmacro)
            self.vstrat.append(newvstrat)

    def setup(self, el, params0, regpar=1e-12, fixparams=None, dparams=None, nproc=None,
              woffset=0.5, model='lip', oversample=1, nlayers=10, verbose=False):
        """
        This subroutine sets up some internal parameters that are necessary
        for the calculations of the spectral regions. In theory we could have
        done this in the __init__ subroutine, but the code is cleaner like this.

        Input parameters:
            el: Name of the chemical element we try to fit

            params0: [eps_u, eps_l, d0, Delta] starting guess for
                the parameters of the vertical profile function.

            fixparams: Which parameters should be fixed. The default
                is to fit all parameters of the model.  In the general case 1
                is fixed, 0 is free.

            dparams: Step for the finite differences procedure.

            nproc: Number of processes to use. The default is
                None, i.e., we get nproc from multiprocessing.cpu_count()

            woffset: Not used.

            nlayers: The minimal number of atmospheric layers for the
                transiton window. This is a technical parameter in order to
                avoid unexpected jumps in the atmosphere model.

            oversample: Overample the model atmosphere on a finer grid in
                lgrhox units using Akima interpolation splines. The new points
                are spread out equidistantly between the original points. The
                default is to oversample the original model atmosphere 5 times.

            model: Selects the type model used to represent the vertical
                stratification gradient. The default is Hermitian piecewise
                polynomial 'hpp', other choices are linear interpolating poly-
                nomial 'lip' and one parameter function for fitting homoge-
                nenous abundace 'homog'.

            verbose: Gets passed to the vprofile functions as a verbose argument.
        """

        #chemical element to fit
        self.zelem = pykrz.get_zelem(el)
        self.aelem = pykrz.get_aelem(self.zelem)

        #process the initial guess, one guess for each phase
        self.params0 = np.asarray(params0, dtype=np.float64) #2d array
        self.nparams = self.params0.shape[1] #number of columns

        #check that we get initial guess for each phase
        assert self.nphases == self.params0.shape[0] #number of rows

        #total number of parameters
        self.ntotalparams = self.nparams * self.nphases

        #set the fixed and free parameters
        #currently cannot be done for only one parameter, you have to
        #apply the same recipe for every phase
        if fixparams is None:
            self.fixparams = np.zeros(self.nparams, dtype=np.int32)
        else:
            self.fixparams = np.asarray(fixparams, dtype=np.int32)
        assert len(self.fixparams) == self.nparams

        #indices of the free and fixed params (for fast access)
        self.ifree = np.where(self.fixparams == 0)[0]
        self.ifixed = np.where(self.fixparams == 1)[0]
        self.nfree = self.ifree.size #number of free parameters

        #set the regularization parameter for each fitted parameter
        if not isinstance(regpar,np.ndarray) or not isinstance(regpar,list):
            self.regpar = np.ones(self.nparams) * regpar
        else:
            self.regpar = np.asarray(regpar)
        assert self.regpar.size == self.nparams

        #setup the individual solvers
        for iphase in range(self.nphases):
            self.vstrat[iphase].setup(self.aelem, self.params0[iphase],
                fixparams=fixparams, dparams=dparams, nproc=nproc, woffset=woffset,
                model=model, oversample=oversample, nlayers=nlayers, verbose=verbose)

        self.verbose = verbose

    def leastsq_function(self, params):
        """
        Returns an array of

            (obs_i - syn_i) * weights_i

        for all wavelength points for all phases supplied through params.

        This is used by the self.leastsq method, params is 1d vector of the
        parameters that are kept free (all phases).
        """
        res = []

        #compute the differences for each phase
        params2d = params.reshape((self.nphases, self.nfree)) #only a view, not a copy
        for iphase in range(self.nphases):
            diff = self.vstrat[iphase].leastsq_function(params2d[iphase])
            res += diff.tolist()
        self.diff = np.asarray(res)
        return self.diff

    def leastsq_jacobian(self, params):
        """
        Returns the Jacobian of

            (obs_i - syn_i) * weights_i

        for all wavelength points with respect to
        the free parameters as given by self.ifree.

        This is used by the self.leastsq method.
        """
        #calculate the jacobian for each phase
        self.jac_all = []
        self.nwl_all = []
        params2d = params.reshape((self.nphases, self.nfree)) #only a view, not a copy
        for iphase in range(self.nphases):
            jac_one = self.vstrat[iphase].leastsq_jacobian(params2d[iphase])
            self.nwl_all.append(jac_one.shape[0]) #each jacobian has its own size
            self.jac_all.append(jac_one)

        #now group them together in one large matrix
        self.jac = np.zeros((np.sum(self.nwl_all), self.nfree*self.nphases))
        istart = 0
        iend = 0
        for iphase in range(self.nphases):
            #row start and end indices
            istart = iend
            iend += self.nwl_all[iphase]
            #now copy data to the full matrix
            self.jac[istart:iend,self.nfree*iphase:self.nfree*(iphase+1)] = self.jac_all[iphase]
        return self.jac

    def leastsq(self, **kwargs):
        """
        Performs non-linear least squares fit for the parameters
        that define how the abundance of a given element changes
        with depth in the atmosphere of a star. 
        
        
        This method calls the following scipy function:
        
        optimize.leastsq(func, x0, args=(), Dfun=None,
            full_output=0, col_deriv=0, ftol=1.49012e-08, xtol=1.49012e-08,
            gtol=0.0, maxfev=0, epsfcn=None, factor=100, diag=None)
            
        Passes arguments to the scipy minimization function through **kwargs.
        """
        #initial guess for the free parameters
        x0 = []
        for iphase in range(self.nphases):
            x0.append(np.copy(self.params0[iphase])[self.ifree])
        x0 = np.asarray(x0).flatten() #1d array

        options = dict(full_output=1,
                       Dfun=self.leastsq_jacobian)
        options.update(**kwargs)
        return optimize.leastsq(self.leastsq_function, x0, **options)

    def _regmatrix(self):
        B = np.zeros((self.nphases*self.nfree - self.nfree, self.nphases*self.nfree), dtype=np.float64)

        for i in range(self.nphases*self.nfree - self.nfree):
            ipar = self.ifree[np.mod(i, self.nfree)]
            B[i,i] = -np.sqrt(self.regpar[ipar])
            B[i,i+self.nfree] = np.sqrt(self.regpar[ipar])
        H = np.dot(np.transpose(B), B)
        self.Bmatrix = B
        self.Hmatrix = H

    def regmatrix(self):
        """
        Creates the regularization matrix B, where
        B = [grad(param1), grad(param2), grad(param3), ..., grad(paramN)],
        while N goes through all of the parameters of a different type.
        These would be for example eps_u, eps_l, d0, Delta (N=4).
        
        The gradient of a parameter is calculated with respect to time, i.e.,
        grad(P)_i = P_{i+1} - P_i, i=0, ..., M-2. M is the number of phases.
        
        For the last point, we have grad(P)_{M-1} = P_{M-1} - P_0, i.e., 
        we connect the last and the first points.
        """
        B = np.zeros((self.nphases*self.nfree, self.nphases*self.nfree), dtype=np.float64)

        # x_{i+1} - x_i, i=0, .., N-2; same as _regmatrix
        for i in range(self.nphases*self.nfree - self.nfree):
            ipar = self.ifree[np.mod(i, self.nfree)]
            B[i,i] = -np.sqrt(self.regpar[ipar])
            B[i,i+self.nfree] = np.sqrt(self.regpar[ipar])

        # connect the first and last points
        for i in range(self.nphases*self.nfree - self.nfree, self.nphases*self.nfree,1):
            ipar = self.ifree[np.mod(i, self.nfree)]
            B[i, np.mod(i, self.nfree)] = -np.sqrt(self.regpar[ipar])
            B[i, i] = np.sqrt(self.regpar[ipar])
        H = np.dot(np.transpose(B), B) #not really needed
        self.Bmatrix = B
        self.Hmatrix = H

    def leastsq_function_reg(self, params):
        """
        Returns as a single array

            (obs_i - syn_i) * weights_i, and
            
            Bmatrix * params

        (all wavelength points for all phases supplied through params).

        This is used by the self.leastsq method, params is 1d vector of the
        parameters that are kept free (all phases).
        """
        df = self.leastsq_function(params)
        dr = np.dot(self.Bmatrix, params)
        self.newdiff = np.asarray(df.tolist() + dr.tolist(), dtype=np.float64)
        return self.newdiff

    def leastsq_jacobian_reg(self, params):
        """
        Returns the Jacobian of

            (obs_i - syn_i) * weights_i and Bmatrix

        for all wavelength points with respect to
        the free parameters as given by self.ifree.

        This is used by the self.leastsq method.
        """
        jac_func = self.leastsq_jacobian(params)

        #self.Bmatrix has to be calculated beforehand.
        self.jac_tot = np.asarray(jac_func.tolist() + self.Bmatrix.tolist(), dtype=np.float64) #rows-wise
        return self.jac_tot

    def leastsq_reg(self, **kwargs):
        """
        Performs non-linear least squares fit for the parameters
        that define how the abundance of a given element changes
        with depth in the atmosphere of a star.
        
        With parameter regularization!!!
        
        
        This method calls the following scipy function:
        
        optimize.leastsq(func, x0, args=(), Dfun=None,
            full_output=0, col_deriv=0, ftol=1.49012e-08, xtol=1.49012e-08,
            gtol=0.0, maxfev=0, epsfcn=None, factor=100, diag=None)
            
        Passes arguments to the scipy minimization function through **kwargs.
        """
        #initial guess for the free parameters
        x0 = []
        for iphase in range(self.nphases):
            x0.append(np.copy(self.params0[iphase])[self.ifree])
        x0 = np.asarray(x0).flatten() #1d array

        options = dict(full_output=1,
                       Dfun=self.leastsq_jacobian_reg)
        options.update(**kwargs)
        
        #prepare the regularization matrices
        self.regmatrix()
        return optimize.leastsq(self.leastsq_function_reg, x0, **options)
