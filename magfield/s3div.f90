! translated rds3.pro and rtint.pro and optimized for fortran90.
! f2py -c s3div.f90 -m _s3div

! fast fortran functions for disk integration of stellar spectra
! works with output from Synmast
module disk_integration
  implicit none
  integer, parameter :: digi = 8
  integer, parameter :: nlen = 300
  real(kind=8), parameter :: pi = 3.14159265358979323846

  contains
  subroutine s3div(nmu, mu, &
                  nwc, wlc, spc, &
                  nwl, wll, spl, &
                  vsini, vrt, resol, rinst, &
                  nx, x, flxl, flxc, &
                  ierror)
  integer, intent(in) :: nmu, nwc, nwl, nx
  real(kind=8), intent(in) :: mu(nmu)
  real(kind=8), intent(in) :: wlc(nwc), spc(nwc,nmu)
  real(kind=8), intent(in) :: wll(nwl), spl(nwl,nmu)
  real(kind=8), intent(in) :: vsini
  real(kind=8), intent(in) :: vrt
  real(kind=8), intent(in) :: rinst
  real(kind=8), intent(in) :: resol
  real(kind=8), intent(out) :: x(nx)
  real(kind=8), intent(out) :: flxc(nx)
  real(kind=8), intent(out) :: flxl(nx)
  integer, intent(out) :: ierror

  ! integration variables
  real (kind=8) :: limbd, icent, error
  real (kind=8) :: meanw, dv
  real (kind=8), allocatable :: c(:)
  real (kind=8), allocatable :: spxl(:,:), spxc(:,:)
  real (kind=8), allocatable :: spmc(:)
  integer :: ios, i

  ierror = 0

  ! setup disc integration
  meanw = ( wlc(nwc) + wlc(1) ) / 2.0
  dv = 2.99792458d5 / resol

  ! allocate the work arrays
  allocate ( spxl(nx,nmu), stat = ios ) 
  if ( ios /= 0 ) then
    ierror = 2 ! "Could not allocate x(:) !"
    return
  endif

  allocate ( spxc(nx,nmu), stat = ios ) 
  if ( ios /= 0 ) then
    ierror = 3 ! "Could not allocate spxl(:) !"
    return
  endif

  allocate ( spmc(nmu), stat = ios ) 
  if ( ios /= 0 ) then
    ierror = 4 ! "Could not allocate spmc(:) !"
    return
  endif

  ! interpolate on logarithmically spaced grid  
  x(1) = wlc(1)
  do i = 2, nx
    x(i) = x(i-1) * ( 1.d0 + 1.d0 / resol )
  enddo
  call lininter(x, spxl, wll, spl, nx, nwl, nmu)
  call lininter(x, spxc, wlc, spc, nx, nwc, nmu)
  call lininter([meanw], spmc, wlc, spc, 1, nwc, nmu)

  ! intensity at the center of the disk
  allocate(c(3))
  call fitting(3, mu, spmc, nmu, c, error)
  icent = c(3) + c(2) + c(1)
  deallocate(c)

  ! limb darkening
  allocate(c(2))
  call fitting(2, mu, spmc, nmu, c, error)
  limbd = c(2) / (c(2)+c(1))
  deallocate(c)
  
  call integrate(nmu, mu, nx, spxl, spxc, dv, vsini, vrt, flxl, flxc)
  if ( rinst > 0 ) call instrument( nx, dv, flxl, flxc, rinst )
end subroutine s3div

subroutine lininter(ax, ay, bx, by, na, nb, angles)
! Linearily interpolate bx,by on ax,ay for all angles
! ax and bx have to be monotonic increasing


 ! Variables
  integer          :: i, j, k, ang
  integer          :: na, nb, angles
  real (kind=8) :: dx, dy
  real (kind=8) :: ax(na), ay(na,angles)
  real (kind=8) :: bx(nb), by(nb,angles)

 ! Execute ( derive ay(j:j) )

  do ang = 1, angles

    k = 1 
    do i = 1, na

      do
        if ( k == nb ) exit
        if ( ax(i) < bx(1) ) exit
        if ( ax(i) >= bx(k) .and. ax(i) < bx(k+1) ) exit
        k = k + 1
      enddo

      if ( k == 1 ) then 
        ! if ax < bx(1) extrapolate
        do j = 2, nb
          if ( bx(j) > bx(1) ) exit
        enddo
        dx = bx(j) - bx(1)
        dy = by(j,ang) - by(1,ang)
        ay(i,ang) = by(1,ang) + (ax(i)-bx(1)) * dy/dx      

      elseif ( k == nb ) then
        ! if ax > bx(nb) extrapolate
        do j = nb, 1, -1
          if ( bx(j) < bx(nb) ) exit
        enddo
        dx = bx(nb) - bx(j)
        dy = by(nb,ang) - by(j,ang)
        ay(i,ang) = by(nb,ang) + (ax(i)-bx(nb)) * dy/dx

      else
        ! if ax > bx(1) and ax < bx(nb) interpolate
        j = k + 1
        dx = bx(j) - bx(k)
        dy = by(j,ang) - by(k,ang)
        ay(i,ang) = by(k,ang) + (ax(i)-bx(k)) * dy/dx
      endif

    enddo

  enddo

end subroutine lininter


subroutine fitting(nm, xin, yin, n, a, e)
! fits 'f(x) = a(nm)*x^(nm-1) + ... + a(2)*x + a(1) ' to  x,y i=1,n


 ! Variables
  integer          :: nm
  integer          :: n, i, j, k
  real (kind=8) :: m(nm,nm+1)
  real (kind=8) :: l(nm,nm), u(nm,nm), b(nm)
  real (kind=8) :: xin(n), yin(n), x(n), y(n) 
  real (kind=8) :: a(nm)
  real (kind=8) :: e, emax, func

 ! Execute

  do i=1, n
    x(i) = xin(i)
    y(i) = yin(i)
  enddo

  ! sort x(i) increasing
  do i = 1, n-1
    do j = i+1, n
      if ( x(i) > x(j) ) then
        e = x(i)
        x(i) = x(j)
        x(j) = e
        e = y(i)
        y(i) = y(j)
        y(j) = e
      endif
    enddo
  enddo
  if ( x(1) == x(n) ) n = 1
 
  ! not enough data points for fitting
  if ( n < nm ) then
    a = 0
    return
  endif 

  ! set up matrix coefficients
  m = 0.0
  do i = 1, nm
    do j = 1, nm
      do k = 1, n
        m(i,j) = m(i,j) + x(k)**(i+j-2)
      enddo
    enddo
  enddo
  do i = 1, nm
    do k = 1, n
      m(i,nm+1) = m(i,nm+1) + y(k) * x(k)**(i-1)
    enddo
  enddo

  ! fit curve to datapoints ( LU method on m(i,j)*a(j)=m(i,4) )
  !                              m = l x u
  ! derive l and u
  l = 0.0
  u = 0.0
  do i = 1, nm
    u(i,i) = 1.0
    l(i,1) = m(i,1)
    u(1,i) = m(1,i) / l(1,1)
  enddo
  do i = 2, nm
    do j = i, nm
      do k = 1, i-1
        l(j,i) = l(j,i) + l(j,k)*u(k,i)
      enddo
      l(j,i) = m(j,i) - l(j,i)
    enddo
    if ( i == nm .and. j == nm ) exit
    do j = i+1, nm
      do k = 1, i-1
        u(i,j) = u(i,j) + l(i,k)*u(k,j)
      enddo
      u(i,j) = ( m(i,j) - u(i,j) ) / l(i,i)
    enddo
  enddo
  ! find  b(4) = L^-1 * m(*,4)
  do i = 1, nm
    b(i) = m(i,nm+1)
    do j = 1, i-1, 1
      b(i) = b(i) - l(i,j) * b(j)
    enddo
      b(i) = b(i) / l(i,j)
  enddo

  ! solve u * x = m(*,4)'
  a = 0.0
  do i = nm, 1, -1
    do j = nm, i+1, -1
      a(i) = a(i) + u(i,j) * a(j)
    enddo
    a(i) = b(i) - a(i)
  enddo

  ! quadratic error
  emax = 0.0d0
  e = ( y(n)-y(1) ) / ( x(n)-x(1) )
  do i = 1, n  
    emax = emax + ( y(i) - ( e*x(i) + y(1) ) )**2
  enddo
  e = 0.0d0
  do i = 1, n
    func = 0.0d0
    do j = 1, nm
      func = func + a(j)*x(i)**(j-1)
    enddo
    e = e + ( y(i) - func )**2
  enddo
  if ( e > emax ) then
    write(*,*) "Problems fitting the data!", e, emax
  endif

end subroutine fitting


subroutine integrate(nang,ang, ni,intl,intc, dv, vsi, mac, fluxl,fluxc)
! integrates inten(ni,nang) over ang. Returns flux(ni). 
 ! Variables
  integer          :: ni, nang
  integer          :: ios, n, i, j, k
  real (kind=8) :: intl(ni,nang), intc(ni,nang), ang(nang)
  real (kind=8) :: fluxl(ni), fluxc(ni)
  real (kind=8) :: dv, vsi, mac, rinst
  real (kind=8) :: rmu(nang), sort(nang)
  real (kind=8) :: r(nang+1), wt(nang)
  integer          :: isort(nang)
  integer (kind=4) :: nm, nmk, nrk
  real (kind=8) :: sigma, sigr, sigt
  real (kind=8) :: arg, ar, at
  real (kind=8) :: norm, v, maxv, r1, r2
  real (kind=8), allocatable :: rkern(:), ypix(:)
  real (kind=8), allocatable :: mkern(:), mrkern(:), mtkern(:)

 ! Execute
 
  ! convert input ang to projected radii (rmu) stellar radius is unit
  ! rmu = sin (angle outward normal and line of sight) = sqrt(1-ang)
  do i = 1, nang
    rmu(i) = sqrt(1.0 - ang(i)**2) 
  enddo
  ! sort projected radii and corresponding intensity into ascending order
  ! (i.e. from disk to center of the limb). Equiv. to ang in descending order
  ! make sorted index vector
  do i = 1, nang
   isort(i) = i
  enddo
  do i = 1, nang-1
    do j = i+1, nang
      if ( rmu(i) > rmu(j) ) then
        k = isort(i)
        isort(i) = isort(j)
        isort(j) = k
      endif
    enddo
  enddo
  ! sort rmu()
  do i = 1, nang
    sort(i) = rmu(i)
  enddo
  do i = 1, nang
    rmu(i) = sort(isort(i))
  enddo
  ! sort angles ( ang() )
  do i = 1, nang
    sort(i) = ang(i)
  enddo 
  do i = 1, nang
    ang(i) = sort(isort(i))
  enddo

  ! sort intensities
  do j = 1, ni
    do i = 1, nang
      sort(i) = intl(j,i)
    enddo
    do i = 1, nang
      intl(j,i) = sort(isort(i))
    enddo
    do i = 1, nang
      sort(i) = intc(j,i)
    enddo
    do i = 1, nang
      intc(j,i) = sort(isort(i))
    enddo
  enddo
  ! calculate projected radii for the boundaries of the disk integration
  ! annuli. ( ??? ob dies stimmt )
  r(1) = 0.0d0
  do i = 2, nang
    r(i) = sqrt( 0.5 * ( rmu(i-1)**2 + rmu(i)**2 ) )
  enddo
  r(nang+1) = 1.0d0
  ! integration weights for each disk integration annulus
  do i = 1, nang
    wt(i) = r(i+1)**2 - r(i)**2
  enddo

  ! determine max # of points for rotation velocity kernel
  nrk = 2 * int( vsi*1.0d0/dv ) + 3
  ! determine # of points for the macroturbulence kernel
  sigma = mac / dv / sqrt(2.0)
  nmk = max( 3, min( int(10*sigma), int((ni-3)/2) ) )
  nm = 2 * nmk + 1

  ! allocate arrays
  allocate( rkern(nrk), stat = ios )
    if ( ios /= 0 ) stop "Could not allocate rkern(:) !"
  allocate( mkern(nm), stat = ios )
    if ( ios /= 0 ) stop "Could not allocate mkern(:) !"
  allocate( mrkern(nm), stat = ios )
    if ( ios /= 0 ) stop "Could not allocate mrkern(:) !"
  allocate( mtkern(nm), stat = ios )
    if ( ios /= 0 ) stop "Could not allocate mtkern(:) !"
  i = max( ni+2*nrk , ni+2*nm )
  allocate( ypix(i), stat = ios )
    if ( ios /= 0 ) stop "Could not allocate ypix(:) !"
  ! make sure flux is zero at start
  do i = 1, ni
    fluxl(i) = 0.0
    fluxc(i) = 0.0
  enddo

  ! loop over annuli

  do n = 1, nang
    ! construct the convolution kernel, which describes the distribution
    ! of rotational velocities present in the current annulus. The 
    ! distribution has been derived analytically for annuli of arbitrary
    ! thickness in a rigidly rotating star. There are 2 kernel pieces:
    ! a) radial velocities less than the maximum velocity along the inner
    !    edge of the annulus.
    ! b) velocities bigger than this limit.
    if ( vsi > 0.0d0 ) then
      r1 = vsi * r(n)
      r2 = vsi * r(n+1)
      maxv = r2
      nrk = 2 * int( maxv / dv ) + 3
      norm = 0.0d0
      do i = 1, nrk
        v = abs( dv * ( i - 1 - (nrk-1)/2 ) )
        if ( v .lt. r1 ) then                  ! low velocity points
           rkern(i) =   sqrt( r2*r2 - v*v )                   &
          &           - sqrt( r1*r1 - v*v )
           norm = norm + rkern(i)
        elseif ( v .le. r2 ) then             ! high velocity points
           rkern(i) =   sqrt( r2*r2 - v*v )
           norm = norm + rkern(i)
        else
           rkern(i) = 0
        endif
      enddo
      do i = 1, nrk  ! normalize kernel
        rkern(i) = rkern(i) / norm
      enddo
      ! convolve the intensity profile with the rotational velocity
      ! kernel for this annulus. Pad each end of the profile with as
      ! many points as are in the convolution kernel. This reduces
      ! the Fourier fringing.
      if ( nrk .gt. 3 ) then
      ! spectrum
        ! pad
        do i = 1, nrk
          ypix(i) = intl(1,n)
        enddo
        do i = nrk+1, nrk+ni
          ypix(i) = intl(i-nrk,n)
        enddo
        do i = nrk+ni+1, 2*nrk+ni
          ypix(i) = intl(ni,n)
        enddo
        ! convolve
        norm = 0.0
        do i = 1, ni
          norm = 0.0d0
          do j = 1, nrk
            k = nrk + i - 1 - int( (nrk-1) / 2 ) + j
            norm = norm + ypix(k) * rkern(j)
          enddo
          intl(i,n) = norm
        enddo
      ! continuum
        ! pad
        do i = 1, nrk
          ypix(i) = intc(1,n)
        enddo
        do i = nrk+1, nrk+ni
          ypix(i) = intc(i-nrk,n)
        enddo
        do i = nrk+ni+1, 2*nrk+ni
          ypix(i) = intc(ni,n)
        enddo
        ! convolve
        norm = 0.0
        do i = 1, ni
          norm = 0.0d0
          do j = 1, nrk
            k = nrk + i - 1 - int( (nrk-1) / 2 ) + j
            norm = norm + ypix(k) * rkern(j)
          enddo
          intc(i,n) = norm
        enddo
      endif
    endif
    ! calculate projected sigma for radial and tangential 
    ! velocity distributions.
    sigr = sigma * ang(n)
    sigt = sigma * sqrt( 1.0 - ang(n)**2 )
    ! construct radial macroturb kernel with sigma = sigr
    if ( sigr .gt. 0.0 ) then
      norm = 0.0
      ! build gaussian
      do i = 1, nm
        arg = max( -20.0d0, -0.5*((i-nmk-1)/sigr)**2 )
        mrkern(i) = exp(arg)
        norm = norm + mrkern(i)
      enddo
      ! normalize
      do i = 1, nm
        mrkern(i) = mrkern(i)/norm
      enddo
    else
      ! build delta function
      do i = 1, nm
        mrkern(i) = 0.0d0
      enddo
      mrkern(nmk+1) = 1.0d0
    endif
    ! construct tangential macroturb kernel with sigt
    if ( sigt .gt. 0.0 ) then
      norm = 0.0
      ! build gaussian
      do i = 1, nm
        arg = max( -20.0d0, -0.5*((i-nmk-1)/sigt)**2 )
        mtkern(i) = exp(arg)
        norm = norm + mtkern(i)
      enddo
      ! normalize
      do i = 1, nm
        mtkern(i) = mtkern(i)/norm
      enddo
    else
      ! build delta function
      do i = 1, nm
        mtkern(i) = 0.0d0
      enddo
      mtkern(nmk+1) = 1.0d0
    endif
    ! sum radial and tangential components weighted by area
    ar = 0.5    ! area covering the radial macroturb
    at = 0.5    ! area covering the tangential macroturb
    do i = 1, nm
      mkern(i) = ar * mrkern(i) + at * mtkern(i)
    enddo
    ! convolve the total flux profiles. Again pad the spectrum
    ! on both ends to prohibit fourier fringing.
                            ! spectrum
    ! pad
    do i = 1, nm
      ypix(i) = intl(1,n)
    enddo
    do i = nm+1, nm+ni
      ypix(i) = intl(i-nm,n)
    enddo
    do i = nm+ni+1, ni+2*nm
      ypix(i) = intl(ni,n)
    enddo
    ! convolve
    do i = 1, ni
      norm = 0.0d0
      do j = 1, nm
        k = nm + i - 1 - int( (nm-1) / 2 ) + j
        norm = norm + ypix(k) * mkern(j)
      enddo
      intl(i,n) = norm
    enddo
                            ! continuum
    ! pad
    do i = 1, nm
      ypix(i) = intc(1,n)
    enddo
    do i = nm+1, nm+ni
      ypix(i) = intc(i-nm,n)
    enddo
    do i = nm+ni+1, ni+2*nm
      ypix(i) = intc(ni,n)
    enddo 
    ! convolve
    do i = 1, ni
      norm = 0.0d0
      do j = 1, nm
        k = nm + i - 1 - int( (nm-1) / 2 ) + j
        norm = norm + ypix(k) * mkern(j)
      enddo
      intc(i,n) = norm
    enddo

    ! add contribution of the current annulus to the total
    do i = 1, ni
      fluxl(i) = fluxl(i) + intl(i,n)
      fluxc(i) = fluxc(i) + intc(i,n)
    enddo

  enddo

  ! normalize flux
  do i = 1, ni
    fluxl(i) = fluxl(i) / nang
    fluxc(i) = fluxc(i) / nang
  enddo

  ! deallocate velocity kernel vectors
  deallocate(rkern)
  deallocate(mkern)
  deallocate(mrkern)
  deallocate(mtkern)
  deallocate(ypix)

end subroutine integrate

subroutine instrument( nw, dw, flxl, flxc, res )
! convolve with instrument profile (res=resolution)
  ! Variables
  integer          :: nw
  real (kind=8) :: dw, res 
  real (kind=8) :: flxl(nw), flxc(nw)
  real (kind=8) :: sigma, arg, norm, normc
  real (kind=8), allocatable :: gauss(:), fl(:), fc(:)
  integer :: i, j, k, ng, ios

  ! Execution
  if ( res < 1.0 ) return

  ! prepare gaussian
  !sigma = 1.0d0 / dw / res
  sigma = 2.99792458d5 / ( res * dw * 2.355d0 )
  j = max( 3, min( int(10.0*sigma) , int((nw-3)/2) ) )
  ng = 2 * j + 1
  allocate( gauss(ng), stat=ios )
  norm = 0.0d0
  do i = 1, ng
    !arg = max( -20.0d0 , -1.0d0*((i-j-1)/sigma)**2 )
    arg = max( -20.0d0 , -0.5d0*((i-j-1)/sigma)**2 )
    gauss(i) = exp(arg)
    norm = norm + gauss(i)
  enddo
  do i = 1, ng
    gauss(i) = gauss(i) / norm
  enddo

  ! padding 
  k = int( (ng-1)/2 )
  j = nw + 2 * k
  allocate( fl(j), stat=ios)
  allocate( fc(j), stat=ios)
  do i = 1, k 
    fl(i) = flxl(1)
    fc(i) = flxc(1)
  enddo
  do i = k+1, k+nw
    fl(i) = flxl(i-k)
    fc(i) = flxc(i-k)
  enddo
  do i = k+nw+1, k+nw+k
    fl(i) = flxl(nw)
    fc(i) = flxc(nw)
  enddo

  ! convolve
  do i = 1, nw
    norm  = 0.0d0
    normc = 0.0d0
    do j = 1, ng
      norm  = norm  + fl(i+j-1) * gauss(j)
      normc = normc + fc(i+j-1) * gauss(j)
    enddo
    flxl(i) = norm
    flxc(i) = normc
  enddo

  deallocate( gauss )
  deallocate( fl )
  deallocate( fc )

end subroutine instrument
end module disk_integration