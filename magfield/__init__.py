"""
This is a collection of various modules and tools that
are most commonly used when working with spectrum synthesis
in stellar spectra that accounts for magnetic fields and
vertical stratification of chemical elements.

The available modules are:
  - pyvald: parses VALD stellar extract spectra line lists,
  - pykrz: parses plane-parallel atmosphere models in Kurucs format,
  - pysynth: computes, processes, and fits synthetic to observed data,
  - pys3div: stellar disk integration of spectra outputted by Synmast,
  - vstrat: solves a vertical abundance stratification problem in
       magnetized stellar spectra.


A number of examples is present in the distribution folder 'examples'
that showcase the capabilities and limitations of this software.

Note: Most of the heavy computational parts depend on the stellar
synthesis code Synmast that is not freely available. The code of
Synmast is not freely available and permission to use it must be
requested from Oleg Kochukhov (oleg.kochukhov@physics.uu.se).
"""

from __future__ import division, print_function, absolute_import

from os import path
package_dir = path.abspath(path.dirname(__file__))


__version__ = '0.4'

__all__ = ['pyvald', 'pykrz', 'pysynth', 'pys3div', 'vstrat']
