"""
Helper module for working with atmospheric models in Kurucs format.

The main functions are read_krz and print_krz.
"""
from __future__ import print_function, absolute_import, division

import numpy as np


# This is how they go as atomic number increases
__elements = ['H ', 'He', 'Li', 'Be', 'B ', 'C ', 'N ', 'O ', 'F ', 'Ne', 'Na',
              'Mg', 'Al', 'Si', 'P ', 'S ', 'Cl', 'Ar', 'K ', 'Ca', 'Sc', 'Ti',
              'V ', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As',
              'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y ', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru',
              'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I ', 'Xe', 'Cs',
              'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy',
              'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W ', 'Re', 'Os', 'Ir',
              'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra',
              'Ac', 'Th', 'Pa', 'U ', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es']

# make a dictionary Z<-->element name
__zelem = dict(zip(range(1, 100, 1), __elements))


def get_zelem(el):
    """
    Returns the atomic number of a chemical element from its symbol as a string.
    """
    for _z, _el in __zelem.viewitems():
        if _el.strip().lower() == el.strip().lower():
            return _z
    raise ValueError("Element %s was not found in the available data.")


def get_aelem(zelem):
    """
    Returns the symbol as a string of a chemical element given its atomic number.
    """
    return __zelem[np.int32(zelem)]


def read_krz(lines):
    """
    Parses an atmospheric model in Kurucs format as an ascii file. 

    If the file has extra columns beyond the standard 5, these will be
    interpreted as abundance profiles of a selection of chemical elements.
    These should be in the format: 'EL: log10(N_el/N_all)'.

    The outpus is a dictionary with keys:
      - title: name of the atmosphere model, specified in the file,
      - Teff: Effective temperature of the atmosphere,
      - logg: logarithm of the surface gravity (in cm/s^2),
      - ModelType: 0 for plane-parallel (used here),
      - wlstd: Standard wavelength,
      - ifop: continuum opacity switches,
      - atable: abundance table (log10(N_el/N_all)),
      - nrhox: number of depth points,
      - rhox: column density in g/cm^2,
      - t: temperature in K,
      - xne: concentration of free electrons (in cm^{-3}),
      - xna: concentration of other particles (in sm^{-3}),
      - rho: density in g/cm^3
      - strat: abundance profiles for a number of chemical elements,
      - nstrat: the number of abundance profiles (relevant for strat).

    Note: The Kurucs format is presented in detail at:
        http://marcs.astro.uu.se/krz_format.html
    """
    # title
    title = lines[0].strip()

    # teff, logg, mtype, wlstd
    line = lines[1].split('=')
    vals = []
    for line_ in line[1:]:
        vals.append(line_.split()[0])
    teff = np.float64(vals[0])
    logg = np.float64(vals[1])
    mtype = np.int32(vals[2])
    wlstd = np.float64(vals[3])

    # opacity switches
    ifop = np.int32(lines[2].split('-')[0].split())

    # parse the abundance table and nrhox
    atable = []
    for line in lines[3:13]:  # ten lines
        atable += line.split()
    nrhox = np.int32(atable.pop(-1))  # the last value is what we need
    atable = np.float64(atable)

    if nrhox < 0:
        raise ValueError("Number of layers cannot be negative.")

    # parse the remaining rows
    rhox = []
    t = []
    xne = []
    xna = []
    rho = []
    for line in lines[13:13+nrhox]:
        rhox_, t_, xne_, xna_, rho_ = line.split(',')[:5]
        rhox.append(np.float64(rhox_))
        t.append(np.float64(t_))
        xne.append(np.float64(xne_))
        xna.append(np.float64(xna_))
        rho.append(np.float64(rho_))

    # If we have additional columns add them to the data
    strat = {}
    line = lines[13].split(',')
    if len(line[-1].strip()) == 0:
        line.pop(-1)  # empty string
    nstrat = len(line) - 5

    if nstrat > 0:  # number of stratification profiles
        # go through line and find which elements have stratification
        for line_ in line[5:]:
            el, abn = line_.replace("'", '').replace("'", "").split(":")
            strat[get_zelem(el)] = []

        # now go through all line with data and read the data
        for line in lines[13:13+nrhox+1]:
            line = line.split(',')
            if len(line[-1].strip()) == 0:
                line.pop(-1)  # empty string
            for line_ in line[5:]:
                el, abn = line_.replace("'", '').replace("'", "").split(":")
                strat[get_zelem(el)].append(np.float64(abn))

    krz = {'title': title,
           'Teff': teff, 'logg': logg, 'ModelType': mtype, 'wlstd': wlstd,
           'ifop': ifop,
           'atable': atable, 'nrhox': nrhox,
           'rhox': rhox, 't': t, 'xne': xne, 'xna': xna, 'rho': rho,
           'strat': strat, 'nstrat': nstrat}

    return krz


def print_krz(krz):
    """
    The input 'krz' is a dictionary outputted by the function read_krz.

    This function outputs the dictionary as an ascii file that can be
    parsed as a relevant Kurucs format atmosphere model.
    """
    lines = []  # results

    # first line
    lines.append("{0[title]}\n".format(krz))

    # second line
    line = "TEFF={0[Teff]:5.0f} \
            GRAV={0[logg]:5.2f} \
            MODEL TYPE= {0[ModelType]:d} \
            WLSTD={0[wlstd]:8.0f}".format(krz)
    lines.append(line+'\n')

    # third line
    line = " {:d}"*len(krz['ifop']) + ' - OPACITY SWITCHES\n'
    lines.append(line.format(*krz['ifop']))

    # abundance table and nrhox
    ilin = 1
    line = "{0[0]:7.3f}{0[1]:7.3f}".format(krz['atable'])
    for abn in krz['atable'][2:]:
        line += "{:7.2f}".format(abn)
        ilin += 1

        if ilin == 9:
            lines.append(line+'\n')
            line = ''
            ilin = -1
    line += " {:d}\n".format(krz['nrhox'])
    lines.append(line)

    # print the main data
    lines_ = []
    for i in range(krz['nrhox']):
        line = "{0:16.9e},{1:9.1f},{2:12.5e},{3:12.5e},{4:12.5e}, ".format(
            krz['rhox'][i], krz['t'][i], krz['xne'][i],
            krz['xna'][i], krz['rho'][i])
        lines_.append(line)

    # print the stratification profiles
    if krz['nstrat'] > 0:
        zelems = krz['strat'].viewkeys()
        aelems = [get_aelem(zelem) for zelem in zelems]  # element names
        for i in range(krz['nrhox']):
            for zelem, aelem in zip(zelems, aelems):
                lines_[i] += "\'{0:2s}:{1:8.4f}\', ".format(aelem,\
                             krz['strat'][zelem][i])

    # add end lines and return results
    for line_ in lines_:
        lines.append(line_+'\n')
    return lines


def read_krzfile(fname):
    """Canned function for parsing Kurucs atmosphere model ascii files"""
    with open(fname) as fin:
        lines = fin.readlines()

    return read_krz(lines)