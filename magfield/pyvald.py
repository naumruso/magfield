"""
Helper module for reading VALD line lists
in mode "stellar" with or without the Zeeman splitting
of the spectral lines.

The main functions are read_slist and read_zlist.
"""
from __future__ import print_function, absolute_import, division

import numpy as np

# The fast Cython functions
from ._pyvald import parse_zlines, parse_slines, get_zelem, get_aelem


# Canned functions for simple use
def parse_file(fname):
    with open(fname) as fin:
        lines = fin.readlines()
    return lines


def read_sfile(fname):
    return read_slist(parse_file(fname))


def read_mfile(fname):
    return read_mlist(parse_file(fname))


def read_zfile(fname):
    return read_zlist(parse_file(fname))


# Header parsers
def parse_header(lines):
    """
    Process the header of a short format VALD stellar extract output. 

    The results are saved in a dictionary with the keys:
      - wstart: start of the wavelength region,
      - wend: end of the wavelength region,
      - nlines_sel: number of selected spectral lines by VALD,
      - nlines_proc: number of processed spectral lines by VALD,
      - vmicro: microturbulent velocity used.
    """
    res = {}

    # first line
    wstart, wend, nlines_sel, nlines_proc, vmicro = lines[0].split(',')[:5]
    res['wstart'] = np.float64(wstart)
    res['wend'] = np.float64(wend)
    res['nlines_sel'] = np.int32(nlines_sel)
    res['nlines_proc'] = np.int32(nlines_proc)
    res['vmicro'] = np.float64(vmicro)

    # third and fourth lines are lines text
    res['text'] = [lines[1], lines[2]]
    return res


def parse_zheader(header):
    """
    Parses the header of a long format VALD stellar extract output data.
    For use with the magnetic synthetis code Synmast. 

    The difference between short and long format is that the latter
    has the magnetic field vector in the first line of the header. 

    The rest of the header is the same as the one for the short format.
    """
    res = {}

    # first line, B = [Br,Bm,Ba]
    res['mfield'] = np.int32(header[0].strip().split(',')[:3]).tolist()

    # the rest of the header is the same as for other files
    res.update(parse_header(header[1:]))
    return res


# Header printers
def print_header(header):
    """
    Take the output by parse_header and output it as a list of 
    strings ready to be printed to a file.
    """
    res = []

    # first line
    txt = 'Wavelength region, lines selected, lines processed, Vmicro'
    line = "{0[wstart]:12.4f},{0[wend]:12.4f}, {0[nlines_sel]:d}, \
            {0[nlines_proc]:d}, {0[vmicro]:.1f}, {1:s}\n".format(header, txt)
    res.append(line)

    # second and third lines
    res += header['text']
    return res


def print_zheader(zheader):
    """
    Take the output by parse_zheader and output it as a list of 
    strings ready to be printed to a file.
    """
    res = []

    # first line
    res.append("{0[0]:d},{0[1]:d},{0[2]:d}\n".format(zheader['mfield']))

    # the rest of the lines
    res += print_header(zheader)
    return res


# main readers
def read_mlist(lines):
    """
    Not implemented
    """
    pass


def read_slist(lines):
    """
    Parses short format VALD stellar extract output for calculation
    of stellar spectra without the effect of a magnetic field. 

    The function returns a dictionary with the keys:
      - header, contains the header of the file,
      - sp_lines, dictionary with arrays that contain data for each line,
      - krz, filename of the Kurucs atmosphere model,
      - atable, the abundance table from the input as outputted by read_atable.

    Note: the line data are processed by parse_slines. 
    """
    # get basic info
    sheader = parse_header(lines)

    # parse line data
    slines, ilin = parse_slines(lines, sheader['nlines_sel'], ilin=3)
    slines['rawdata'] = lines[3:ilin]

    # the atmosphere model filename should be on the next line
    # after the spectral lines
    krz = lines[ilin].strip().split(',')[0]
    krz = krz.replace('\'', '').replace('\"', '')

    # parse the abundance table should be located after the atmosphere model
    atable = parse_atable(lines, ilin=ilin+1)

    # resulting dictionary
    return {'header': sheader, 'sp_lines': slines, 'krz': krz, 'atable': atable}


def read_zlist(lines):
    """
    Read long format VALD stellar extract files with zeeman splitting pattern
    for the spectral lines. For calcualtions of magnetic spectra 
    with Synmast.

    The function returns a dictionary with the keys:
      - header, contains the header of the file,
      - sp_lines, dictionary with arrays that contain data for each line,
      - krz, filename of the Kurucs atmosphere model,
      - atable, the abundance table from the input as outputted by read_atable.

    Note: the line data are parsed by parse_zlines.
    """
    # parse the header
    zheader = parse_zheader(lines)

    # parse line data
    zlines, ilin = parse_zlines(lines, zheader['nlines_sel'], ilin=4)

    # the atmosphere model filename should be on the next line
    # after the spectral lines
    krz = lines[ilin].strip().split(',')[0]
    krz = krz.replace('\'', '').replace('\"', '')

    # parse the abundance table. This should be located after
    # the atmosphere model
    atable = parse_atable(lines, ilin=ilin+1)

    # resulting dictionary
    return {'header': zheader, 'sp_lines': zlines, 'krz': krz, 'atable': atable}


# main printers
def print_slist(slist, krz=None):
    """
    Take the output by parse_slist or read_sfile and output
    it as a list of strings ready to be printed to a file.

    You can also supply a different name for the atmosphere model.
    """
    # header
    lines = print_header(slist['header'])

    # line data
    lines += slist['sp_lines']['rawdata']

    # update model atmosphere file
    if krz is None:
        lines.append("'{0[krz]:s}',\n".format(slist))
    else:
        lines.append("'{}',\n".format(krz))

    # print the abundance table
    lines += print_atable(slist['atable'])
    return lines


def print_zlist(zlist, krz=None):
    """
    Take the output by zread_zlist or read_zfile and output
    it as a list of strings ready to be printed to a file.

    You can also supply a different name for the atmosphere model.
    """
    # header
    lines = print_zheader(zlist['header'])

    # line data
    for line in zlist['sp_lines']['rawdata']:
        lines += line

    # update model atmosphere file
    if krz is None:
        lines.append("'{0[krz]:s}',\n".format(zlist))
    else:
        lines.append("'{}',\n".format(krz))

    # print the abundance table
    lines += print_atable(zlist['atable'])
    return lines

def print_inv(zlist):
    """Useful for printing zfiles in format readable by INVERS10"""

    Nlines = 0
    lines = []
    for i in range(zlist['header']['nlines_sel']):
        el = zlist['sp_lines']['el'][i]
        ion = zlist['sp_lines']['ion'][i]
        cwave = zlist['sp_lines']['cwave'][i]
        excit = zlist['sp_lines']['excit'][i]
        loggf = zlist['sp_lines']['loggf'][i]
        rad = zlist['sp_lines']['rad'][i]
        stark = zlist['sp_lines']['stark'][i]
        waals = zlist['sp_lines']['waals'][i]
        lande = zlist['sp_lines']['lande'][i]

        # skip lines with missing splitting information
        # example: Hydrogen lines
        if lande > 0.0: continue

        sfmt = "\'{:2s} {:1d}\',{:10.4f},{:8.4f},{:8.3f},{:8.3f},{:8.3f},{:8.3f}, 0\n"
        line = sfmt.format(el, ion, cwave, excit, loggf, rad, stark, waals)
        lines.append(line)
        lines += zlist['sp_lines']['rawdata'][i][1:]

        Nlines += 1

    #output list
    lines = ['Nlines={:d},\n'.format(Nlines)] + lines
    return lines

# abundance table functions
def __parse_atable(lines, ilin=0, nelements=99):
    """Deprecated"""
    elements = []

    for i in range(ilin, len(lines), 1):
        if lines[i].split(',')[-1].lower().find('end') != -1:
            break
    istart = i
    iend = i

    cline = lines[istart].rstrip().lstrip().split(',')[:-1]
    while nelements > 0:
        elements += cline[-1::-1]
        nelements -= len(cline)
        istart -= 1

        cline_full = lines[istart].rstrip().lstrip().split(',')
        cline = []
        for cl in cline_full:
            if len(cl) != 0:
                cline.append(cl)
    return elements[-1::-1]


def __print_atable(elements, nelements=99):
    """Deprecated"""
    if len(elements) != nelements:
        raise ValueError('Incomplete abundance table. nelements != 99')

    lines = []
    lines.append("{0[0]:s},{0[1]:s}\n".format(elements))

    j = 0
    line = ''
    for i in range(2, nelements, 1):
        line += "{0[i]:s},".format(elements)
        j += 1

        if j == 6:
            lines.append("{}\n".format(line))
            line = ''
            j = 0
    lines.append("{}'END'\n".format(line))
    return lines


def parse_atable(lines, ilin=0, nelements=99):
    """
    The input is a list of strings with the abundance
    values for each chemical element. The format of the
    abundance table is the following:
    ' H':value, 'He':value, ..., 'END'

    If the 'END' marker isn't available the fuction 
    calls an IOError exception.

    The result is a dictionary that uses the atomic number
    of each chemical element as a key under which the
    abundance value is saved, i.e., atable[8] gives the
    abundance of Oxygen. Use the functions get_aelem and
    get_zelem to convert between atomic numbers and 
    the chemical designations of the elements.
    """
    # security check. make sure that the 'END' marker is
    # present in the configuration file
    end = False
    for i in range(ilin, len(lines), 1):
        for block in lines[i].replace("'", "").split(','):
            if block.strip().lower() == 'end':
                end = True  # end has been found
                break
        if end:
            break

    if not end:
        raise IOError("The 'END' marker not found. Stopping.")

    # start from lines[ilin] and read the abundance table until
    # you reach the marker 'END'
    elements = []
    end = False
    for line in lines[ilin:]:
        for block in line.split(','):  # split current line into segments
            el = block.replace("'", "").strip()

            # check if el is an empty string
            if len(el) == 0:
                continue

            # check if el is the stop point
            if el.lower() == 'end':
                end = True
                break
            else:
                # print block.strip()
                elements.append(el)  # possible element

            if len(elements) > nelements:
                raise IOError('The abundance table in your select3 \
                              configuration file has more than {:d} \
                              elements.'.format(nelements))

        # check if end has been found
        if end:
            break

    # make a dictionary out of what has been found
    # this serves as another step that the input is correct
    atable = {}
    for element in elements:
        key, value = element.split(":")
        atable[get_zelem(key)] = float(value)
    return atable


def print_atable(atable):
    """
    Print abundance table.
    """
    lines = []

    # first print Hydrogen and Helium in one line
    line = ''
    if 1 in atable:
        line += "\'H :{0[1]:6.2f}\',".format(atable)

    if 2 in atable:
        line += "\'He:{0[2]:6.2f}\',\n".format(atable)
    lines.append(line)

    # now print the other elements
    j = 0
    line = ''
    for zkey in range(3, 100, 1):
        if zkey in atable:
            line += "\'{0:2s}:{1:6.2f}\',".format(get_aelem(zkey), atable[zkey])
            j += 1

        if j == 6:
            lines.append("{}\n".format(line))
            line = ''
            j = 0
    lines.append("{}\'END\'\n".format(line))
    return lines
