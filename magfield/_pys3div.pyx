from __future__ import print_function

cimport cython

import numpy as np
cimport numpy as np

def read_mout(list lines, int nst=1):
    """
    By default reads only the first nwl spectral points.
    Change nst to read the other Stokes parameters if
    they are present in the calculations.

    input: lines, nst
    output: mu, wlc, spc, wll, spl
    """
    cdef int nmu, nwc, nwl, nlin
    cdef np.ndarray[np.float64_t, ndim=1] mu, wlc, wll
    cdef np.ndarray[np.float64_t, ndim=2] spc
    cdef np.ndarray[np.float64_t, ndim=3] spl

    cdef object line
    cdef int i, j, iwl, ilin

    # read the number of spectral lines
    nlin = int(lines[0].split('-')[0])

    # we start from here in the list
    ilin = nlin + 1

    # number of angles
    line = lines[ilin]
    nmu = int(line.strip())
    mu = np.zeros(nmu, dtype=np.float64)
    ilin += 1

    # read the angles
    line = lines[ilin].split()
    for i in range(nmu):
        mu[i] = float(line[i])
    ilin += 1

    # read the number of wavelength points for the continuum intensity
    # and create the arrays
    nwc = int(lines[ilin])
    wlc = np.zeros(nwc, dtype=np.float64)
    spc = np.zeros((nwc,nmu), dtype=np.float64, order='F')
    ilin += 1

    for i in range(nwc):
        line = lines[ilin].split()
        wlc[i] = float(line[0])
        for j in range(nmu):
            spc[i,j] = float(line[1+j])
        ilin += 1

    # read the number of wavelength points in the line spectrum
    nwl = int(lines[ilin])
    wll = np.zeros(nwl, dtype=np.float64)
    spl = np.zeros((nst,nwl,nmu), dtype=np.float64, order='F')
    ilin += 1

    for i in range(nst):
        for iwl in range(nwl):
            line = lines[ilin].split()
            wll[iwl] = float(line[0])
            for j in range(nmu):
                spl[i,iwl,j] = float(line[1+j])
            ilin += 1
    return mu, wlc, spc, wll, spl