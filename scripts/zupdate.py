#!/usr/bin/env python

import sys
import argparse

import numpy as np

from magfield import pyvald


def update_wrange(zlist, wstart, wend):
    """
    Makes the wavelength selection using (wstart, wend).

    The selection is then applied ONLY to zlist['sp_lines']['rawdata'].

    If wstart and wend are present zlist['header']['wstart'] and
    zlist['header']['wend'] are updated with the new values as well.
    """
    cwave = np.asarray(zlist['sp_lines']['cwave'])
    idx, = np.where((cwave >= wstart) & (cwave <= wend))

    # for the printing we only care about the raw data
    rawdata = np.asarray(zlist['sp_lines']['rawdata'])
    zlist['sp_lines']['rawdata'] = rawdata[idx].tolist()

    # update the header
    zlist['header']['wstart'] = wstart
    zlist['header']['wend'] = wend
    zlist['header']['nlines_sel'] = idx.size


parser = argparse.ArgumentParser()
parser.add_argument("infile", type=argparse.FileType('r'), nargs='?',
                    default=sys.stdin, help='filename of the input line list')

parser.add_argument("-o", "--outfile", dest='outfile', nargs='?',
                    type=argparse.FileType('w'), default=sys.stdout,
                    help='output file')

parser.add_argument("-w", "--wrange", dest='wrange', type=float, nargs=2,
                    help='new wavelength limits')

parser.add_argument("-b", "--magfield", dest='magfield', type=float, nargs=3,
                    help='update the magnetic field configuration [Br,Bm,Ba]')

parser.add_argument("-k", "--krz", dest='krz', type=str,
                    help='update the model atmosphere filename')

parser.add_argument("-e", "--elements", dest='elements', type=str, nargs='*',
                    help='element names')

parser.add_argument("-a", "--abundances", dest='abundances', type=float,
                    nargs='*', help='abundance values')

# parse the line arguments
args = parser.parse_args()


# read the contents of the zfile
lines = args.infile.readlines()
zlist = pyvald.read_zlist(lines)
args.infile.close()


# extract the wavelength range
if args.wrange is not None:
    update_wrange(zlist, args.wrange[0], args.wrange[1])


# update the abundance table
if args.elements is not None and args.abundances is not None:
    # check lengths of lists
    if len(args.elements) != len(args.abundances):
        raise StandardError('Number of specified elements and \
                             abundance values do not match.')

    for el, ab in zip(args.elements, args.abundances):
        zelem = pyvald.get_zelem(el)
        zlist['atable'][zelem] = float(ab)

# update the magnetic field configuration
if args.magfield is not None:
    zlist['header']['mfield'] = np.int32(args.magfield).tolist()

# update the krz model atmosphere file name
if args.krz is not None:
    zlist['krz'] = "{:s}".format(args.krz)

# print the results
lines = pyvald.print_zlist(zlist)
args.outfile.writelines(lines)
args.outfile.close()
