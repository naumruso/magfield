#!/usr/bin/env python

import sys
import argparse

import numpy as np

from magfield import pyvald

def zmerge(zfiles):
    # read the first file
    lines = zfiles[0].readlines()
    zlist = pyvald.read_zlist(lines)

    # now merge the rest of the lists with the first one
    for zfile in zfiles[1:]:
        lines = zfile.readlines()
        zlist_tmp = pyvald.read_zlist(lines)

        # For speed the full dictionary isn't remade every time
        # we only append new data to the rawdata list
        zlist['sp_lines']['rawdata'] += zlist_tmp['sp_lines']['rawdata']

        # update wstart and wend
        wstart0 = zlist['header']['wstart']
        wend0 = zlist['header']['wend']

        wstart1 = zlist_tmp['header']['wstart']
        wend1 = zlist_tmp['header']['wend']

        zlist['header']['wstart'] = np.min([wstart0, wstart1])
        zlist['header']['wend'] = np.max([wend0, wend1])

        # update the number of lines
        zlist['header']['nlines_sel'] += zlist_tmp['header']['nlines_sel']
        zlist['header']['nlines_proc'] += zlist_tmp['header']['nlines_proc']
    return zlist

description = "Concatenate two or more Zeeman file lists."
epilog = "All parameters of the output are interited from the first input file."

parser = argparse.ArgumentParser(description=description, epilog=epilog)
parser.add_argument("infiles", type=argparse.FileType('r'), nargs='*',
                    help='Input line lists')

parser.add_argument("-o", "--outfile", dest='outfile', nargs='?',
                    type=argparse.FileType('w'), default=sys.stdout,
                    help='Output file. Default is stdout.')

# parse the line arguments

args = parser.parse_args()

# checks
if len(args.infiles) < 2:
    raise StandardError("You need two or more files for concatenation.")

# main part
zlist = zmerge(args.infiles)

# print the results
lines = pyvald.print_zlist(zlist)
args.outfile.writelines(lines)
args.outfile.close()