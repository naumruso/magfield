#!/usr/bin/env python

import sys
import argparse

import numpy as np

from magfield import pyvald


def zselect(zlist, el=None, ions=None, wstart=None, wend=None, climit=None):
    """
    Makes the wavelength selection using (wstart, wend), and
    for the given element (el) and ionization states (ions), and depth limit.
    """
    cwave = np.asarray(zlist['sp_lines']['cwave'])
    Z = np.asarray(zlist['sp_lines']['Z'])
    ion = np.asarray(zlist['sp_lines']['ion'])
    cdepth = np.asarray(zlist['sp_lines']['cdepth'])

    # initial condition array
    cond = np.array([True]*cwave.size, copy=True)

    # wavelength selection
    if wstart is not None and wend is not None:
        cond &= ((cwave >= wstart) & (cwave <= wend))

    # element selection
    if el is not None:
        cond &= (Z == pyvald.get_zelem(el))

    # ions selection
    if ions is not None:
        cond_ion = (ion == ions[0])
        for idx in range(1,len(ions),1):
            cond_ion |= (ion == ions[idx])
        cond &= cond_ion

    # cdepth selection
    if climit is not None:
        cond &= (climit <= cdepth)

    idx, = np.where(cond)
    return idx

def print_inv(zlines, idx):
    """Prints zlist for the given indices in idx in INVERS10 format"""
    # Below we start the printing part
    lines = []
    lines.append('Nlines= {:d},\n'.format(idx.size))

    for idx_ in idx:
        el = zlist['sp_lines']['el'][idx_]
        ion = zlist['sp_lines']['ion'][idx_]
        cwave = zlist['sp_lines']['cwave'][idx_]
        excit = zlist['sp_lines']['excit'][idx_]
        loggf = zlist['sp_lines']['loggf'][idx_]
        rad = zlist['sp_lines']['rad'][idx_]
        stark = zlist['sp_lines']['stark'][idx_]
        waals = zlist['sp_lines']['waals'][idx_]
        lande = zlist['sp_lines']['lande'][idx_]

        # skip lines with missing splitting information
        # example: Hydrogen lines
        if lande > 0.0: continue

        sfmt = "\'{:2s} {:1d}\',{:10.4f},{:8.4f},{:8.3f},{:8.3f},{:8.3f},{:8.3f}, 0\n"
        line = sfmt.format(el, ion, cwave, excit, loggf, rad, stark, waals)
        lines.append(line)
        lines += zlist['sp_lines']['rawdata'][idx_][1:]
    return lines

parser = argparse.ArgumentParser()
parser.add_argument("infile", type=argparse.FileType('r'), nargs='?',
                    default=sys.stdin, help='filename of the input line list')

parser.add_argument("-o", "--outfile", dest='outfile',
                    type=argparse.FileType('w'), nargs='?',
                    default=sys.stdout, help='output file')

parser.add_argument("-w", "--wrange", dest='wrange', type=float, nargs=2,
                    help='new wavelength limits')

parser.add_argument("-b", "--magfield", dest='magfield', type=float, nargs=3,
                    help='update the magnetic field configuration [Br,Bm,Ba]')

parser.add_argument("-k", "--krz", dest='krz', type=str,
                    help='update the model atmosphere filename')

parser.add_argument("-e", "--element", dest='element', type=str, nargs='*',
                    help='element name')

parser.add_argument("-i", "--ions", dest='ions', type=int, nargs='*',
                    help='ionization states (>=1)')

parser.add_argument("-c", "--climit", dest='climit', type=float, nargs=1,
                    help='Select lines with residual depth >= climit')

parser.add_argument("--inv", dest='inv', action='store_true', default=False,
                    help='Print output file compatible with INVERS10')

# parse the line arguments
args = parser.parse_args()

# read the contents of the zfile
lines = args.infile.readlines()
zlist = pyvald.read_zlist(lines)
args.infile.close()

if args.wrange is None:
    args.wrange = [None, None]

if args.element is None:
    args.element = [None]

# make the selection
idx = zselect(zlist, args.element[0], args.ions, args.wrange[0],
              args.wrange[1], args.climit)

if args.inv:
    lines = print_inv(zlist, idx)
else:
    rawdata = np.asarray(zlist['sp_lines']['rawdata'])
    zlist['sp_lines']['rawdata'] = rawdata[idx].tolist()

    # update the header with the new number of lines
    zlist['header']['nlines_sel'] = idx.size

    # update the magnetic field configuration
    if args.magfield is not None:
        zlist['header']['mfield'] = np.int32(args.magfield).tolist()

    # update the krz model atmosphere file name
    if args.krz is not None:
        zlist['krz'] = "{:s}".format(args.krz)

    # update the wavelength ranges
    if args.wrange[0] is not None and args.wrange[1] is not None:
        zlist['header']['wstart'] = args.wrange[0] - 0.5
        zlist['header']['wend'] = args.wrange[1] + 0.5

    # print in Syntmast format
    lines = pyvald.print_zlist(zlist)

# save results
args.outfile.writelines(lines)
args.outfile.close()
