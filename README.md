# Introduction

The Python module `magfield` emerged as a set of tools and
useful subroutines during my time at Uppsala University where
I worked with spectropolarimetric observations of magnetic
chemically peculiar stars Ap and Bp stars. 

The tools in `magfield` do not aim at creating an integrated
work environment for the analysis of spectroscopic observations
of magnetic stars, that title goes to the IDL visualisation tool
[`BinMag`](http://www.astro.uu.se/~oleg/binmag.html) produced and actively supported by Oleg Kochukhov. 

Instead, the `magfield` package attempts to automate and simplify
a few commonly performed tasks while analysing spectroscopic
observations of magnetic stars, and in particular the comparison
of observations to synthetic data.

The tasks that `magfield` excells at are:

1. Fast manipulation of long format [VALD](http://vald.astro.uu.se) stellar extract data.
Some of these tasks include splitting a VALD line list into
several smaller line lists, extraction of line data according to
some predefined criteria as well as modifying the header of 
long format VALD line lists. A complete detailed description of
the scripts that allow these operations, together with a number of
examples that illustrate some common tasks can be found in the
folder `examples/scripts` and the `readme.md` file therein.

2. Automatic estimation of abundance of chemical elements from
a user supplied list of wavelength ranges and observed data. 
A short description of the corresponding module and how it can be
used is available in `examples/pysynth/readme.md`.

3. The module `vstrat` focuses on solving of the parametrized
abundance stratification problem for magnetic chemically peculiar
Ap stars from spectroscopic observations. A description of the 
problem and a short example can be found in the folder 
`examples/vstrat` and the file `readme.md` therein.
