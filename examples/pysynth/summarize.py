import os
import sys
import yaml

import numpy as np

from astropy import table
from collections import Counter


def stand_mad(x, c=0.67448975019608171, axis=None):
    """
    The standardized Median Absolute Deviation along given axis of an array.

    Parameters
    ----------
    x : array-like
      Input array.
    c : float, optional
      The normalization constant.  Defined as scipy.stats.norm.ppf(3/4.),
      which is approximately .6745.
    axis : int, optional
      The defaul is 0 (None).

    Note: I pretty much copied the code from statsmodels library.
    """
    return np.median(np.abs(x - np.median(x, axis=axis)), axis=axis) / c

def zvalue(x):
    return np.abs(0.6745*(x - np.median(x))/stand_mad(x))

def calc_means(arr):
    if len(arr) >= 2:
        (mean, std, med, smad) = (np.mean(arr),np.std(arr,ddof=1), np.median(arr),stand_mad(arr))
    elif len(arr) == 1:
        (mean, std, med, smad) = (arr[0],0.0, arr[0],0.0)
    else:
        (mean, std, med, smad) = (0.0,0.0, 0.0,0.0)
    return (mean, std, med, smad)

def estimate_abn(mylines, el, ion=1, fout=sys.stdout):
    cwave = []
    abn = []
    for myline in mylines[el]:
        if myline['select'] and myline['ion'] == ion:
            abn.append(myline['abn'])
            cwave.append(myline['cwave'])
    abn = np.asarray(abn, dtype=np.float64)
    cwave = np.asarray(cwave, dtype=np.float64)
    mean, std, med, smad = calc_means(abn)

    #calculate the z-scores
    if abn.size > 1:
        zvalues = zvalue(abn)
    else:
        zvalues = abn * 0.0

    # now print the long report
    elements = [el]*abn.size
    ions = [ion]*abn.size

    tbl = table.Table(names=['el', 'ion', 'cwave', 'abn', 'z-score'],
                      dtype=['a2', 'i1', 'f8', 'f8', 'f8'],
                      data=[elements, ions, cwave, abn.round(2), zvalues.round(2)])
    tbl.write(fout, format='ascii.fixed_width_two_line', formats={'cwave':'%9.4f'})

    fout.write("Nlines = {:d}\n".format(abn.size))
    fout.write("<{:s}{:1d}>  = {:6.2f} +/- {:4.2f}\n".format(el,ion,mean,std))
    fout.write("M({:s}{:1d}) = {:6.2f} +/- {:4.2f}\n".format(el,ion,med,smad))
    fout.write("\n")
    fout.write("\n")
    return mean, std, med, smad, abn.size

#program parameters
outdir = '.'

# file with the results from abundance.py
savefile = os.path.join(outdir, 'result.fit.example.lines.yaml')

with open(savefile) as fin:
    mylines = yaml.load(fin.read())

# open files for output
freport = open(os.path.join(outdir, 'result.example.report.long.txt'), 'w')
fsummary = open(os.path.join(outdir, 'result.example.report.summary.txt'), 'w')

# the simple report is just a basic table
tbl = table.Table(names=['el', 'ion', 'mean', 'std', 'med', 'smad', 'nlines'],
                  dtype=['a2', 'i1', 'f4', 'f4', 'f4', 'f4', 'i4'])

# process results
for el in mylines.keys():
    #get ion species for the selected lines
    ions = np.asarray([myline['ion'] for myline in mylines[el]])
    selects = np.asarray([myline['select'] for myline in mylines[el]])
    idx, = np.where(selects == True)
    for ion in np.unique(ions[idx]):
        mean, std, med, smad, nlines = estimate_abn(mylines, el, ion=ion, fout=freport)
        tbl.add_row([el, ion, mean, std, med, smad, nlines])

# now save the short summary
formats = {'el':'%2s', 'ion':'%1d', 'mean':'%6.2f', 'std':'%4.2f', 'med':'%6.2f', 'smad':'%4.2f'}
tbl.write(fsummary, format='ascii.fixed_width_two_line', formats=formats)

# close the file for the long report
freport.close()

# print some info
print("Long report of analyzed data: {}".format(os.path.join(outdir, 'result.example.report.long.txt')))
print("Summary of analyzed data: {}".format(os.path.join(outdir, 'result.example.report.summary.txt')))


