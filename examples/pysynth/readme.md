# Description of the pysynth example

The main goal of this example is to estimate the chemical abundance
for a number of chemical elements from observed spectrum of
a magnetic star, for which we already know the strength of its
magnetic field.

The code is a thin wrapper over the magnetic stellar synthesis code
`Synmast` by Oleg Kochukhov (oleg.kochukhov@physics.uu.se), which is
needed for the calculations to be performed.


## `abundance.py`

This script performs the neccessary book keeping as well as the 
plotting of the results and the processing of the measured values. 

The main input files are the following:
  - `example.3780-6920.z`: a VALD stellar extraction in long format
    suitable for magnetic synthesis
  - `example.lines.yaml`: a description of every wavelength range that
    we want to fit, it also can contain values that already have 
    been measured through other means
  - `example.atmosphere.krz`: the necessary model atmosphere in 
    Kurucs format
  - `example.spectrum.obs`: a wavelength and intensity table which
    contains the observations of the spectrum of the target object

The main output files are the following:
  - result.example.lines.pdf: a pdf file with a plot of every
    spectral line that has been processed by the code
  - result.fit.example.lines.yaml: a yaml file with all the 
    results from the fitting of the spectral lines.

To run the code execute the following command in the prompt:

`python abundance.py`


## `summarize.py`

The second script `summarize.py` accepts as input the file with 
the results that is outputted by `abundance.py` and performs an
additional statistical analysis over all measurements for every
chemical element and its ionization state. The results from this
script are `result.example.report.long.txt`, which is a report of
the measurements in long format that describe the each abundance
estimate, and `result.example.report.summary.txt` which is a more
human-readable form that summarizes the measurements to give out
a single value for each chemical element.

To run the code execute the following command in the prompt:

`python summarize.py`

