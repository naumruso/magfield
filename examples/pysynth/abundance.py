from __future__ import print_function

import os
import sys
import yaml
import copy

import numpy as np

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

from astropy.table import Table

from magfield import pyvald, pysynth


def radvelcorr(wave, radvel):
    """
    Positive radvel DECREASES wavelength.
    THIS IS COMPATIBLE WITH DATA FROM SIMBAD.
    """
    return wave * (1.0 + radvel / 2.9979246e5)

def plot_line(mfit, myline, el, fig):
    ax = fig.get_axes()[0]
    ax.clear()

    wstart, wend = myline['wrange']
    cwave = myline['cwave']
    ion = myline['ion']

    # select the relevant line data for the plot and prepare the text labels
    idx, = np.where((mfit.default['cwave'] >= wstart) & (mfit.default['cwave'] <= wend))
    zlines_cwave = mfit.default['cwave'][idx]-cwave
    zlines_flux = np.interp(zlines_cwave, mfit.syn['wl']-cwave, mfit.syn['sp'])
    zlines_label = []
    zlines_cdepth = []
    for i in idx:
        line = "{:2s}{:1d} {:9.4f}".format(mfit.zdata['sp_lines']['el'][i],
                                           mfit.zdata['sp_lines']['ion'][i],
                                           mfit.default['cwave'][i])
        zlines_label.append(line)
        zlines_cdepth.append(mfit.zdata['sp_lines']['cdepth'][i])
    zlines_cdepth = np.asarray(zlines_cdepth)

    # plot spectra
    ax.plot(mfit.obs['wl']-cwave, mfit.obs['sp'], color='black', linewidth=2)
    ax.plot(mfit.syn['wl']-cwave, mfit.syn['sp'], color='red', linewidth=2)
    ax.set_xlabel(r"$\lambda - \lambda_0$")
    ax.set_ylabel("Intensity")
    ax.set_title("{:s}{:2d} {:9.4f} fit={}".format(el,ion,cwave,myline['fit']))

    # plot annotations and results
    xlims = (wstart-cwave, wend-cwave)
    ax.set_xlim(xlims)
    ylims = ax.get_ylim()
    ylims_new = (ylims[0], ylims[0] + (ylims[1]-ylims[0])*1.35)
    ax.set_ylim(ylims_new)
    fig.tight_layout()

    # in macos we need to first plot the data before matplotlib
    # can generate the actual measurements on which pylineid depends.
    if sys.platform == 'darwin':
        fig.show()

    # now annotate the spectral lines
    ypos2 = ylims[1]
    ypos3 = ylims[0] + (ylims[1]-ylims[0])*1.10
    lines_plot = pylineid.put_lines(ax, zlines_cwave, zlines_flux, ypos2, ypos3,
                                    zlines_label, zlines_cdepth,
                                    textkwargs={'size':8}, adjustkwargs={'adjust_factor':0.1})

    # elements (minimization parameters)
    elements = [el]
    res = [myline['abn']]
    xtext = xlims[0] + 0.05*(xlims[1] - xlims[0])
    ytext1 = ylims_new[0] + 0.15*(ylims_new[1]-ylims_new[0])
    ytext2 = ylims_new[0] + 0.12*(ylims_new[1]-ylims_new[0])
    stext1 = str("%5s "*len(elements) %tuple(elements))
    stext2 = str("%5.2f "*len(res) %tuple(res))
    ax.text(xtext,ytext1,stext1, ha='left', va='center', color='red', family='monospace')
    ax.text(xtext,ytext2,stext2, ha='left', va='center', color='red', family='monospace')

    # atable (fixed parameters)
    # xtext = xlims[0] + 0.05*(xlims[1] - xlims[0])
    # ytext1 = ylims_new[0] + 0.08*(ylims_new[1]-ylims_new[0])
    # ytext2 = ylims_new[0] + 0.05*(ylims_new[1]-ylims_new[0])
    # stext1 = str("%5s "*len(atable) %tuple(atable.viewkeys()))
    # stext2 = str("%5.2f "*len(atable) %tuple(atable.viewvalues()))
    # ax.text(xtext,ytext1,stext1, ha='left', va='center', color='black', family='monospace')

def verify_lines(mylines):
    """
    The data used for fitting the spectral lines are saved in yaml format. This function
    reads the data output from yaml.load and processes the line data so that all data
    points are homogeneous. The code also makes sure that we don't have any data points
    that do not conform to the established standards of how we represent these data. 

    The format of the file is:
    element1:
      - line1
      - line2
      ...

    element2:
      - line1
      - line2
      ...
    ...

    elementM:
      - line1
      - line2
      ...

    where element1, element2, ..., elementM, is the symbol of each chemical element
    for which we are fitting the spectral lines, and

    each line1, line2, ..., object is a dictionary with the following fields:
      - abn: initial abundance value, otherwise use the one from the supplied model atmosphere
      - comment: a random text comment
      - ion: ionization state of the chemical element (1 is neutral, 2 is once ionized, etc.)
      - select: boolean value, which if set to true marks the line to be used for abundance determination
      - fit: boolean value, which if set to true tells the code to fit the line
      - cwave: central wavelength (in Angstrom)
      - mfield: magnetic field vector default: [7200, 0, 0]
      - cdepth: central depth
      - vcorr: corrected radial velocity
      - renorm: renormalize the continuum around the line
      - wrange: wavelength range for the fitted spectral line
      - obligatory: [cwave, ion] a list of obligatory fields

    Note that most of these fields are not obligatory with the exception of cwave, and ion. 
    If 'fit' is False then the field 'abn' is also obligatory if the field 'select' is true.
    """
    default = mylines.pop('default') # default value for each field
    newlines = {}

    # a loop around each chemical element
    for el in mylines.keys():
        newlines[el] = []

        # a loop for each line of the given element
        for myline in mylines[el]:
            newline = default.copy()
            newline.update(myline)

            # check the obligatory fields
            for key in newline['obligatory']:
                newline
                if newline[key] is None:
                    raise KeyError('Key {} is missing for {} line: {}'.format(key,el,newline))

            # check the implicit fields
            # we can't fit a spectral line without having a specified wavelength range
            newline['fit'] = newline['wrange'] is not None and newline['fit']

            # manual fit requires key abn to be present
            if not newline['fit'] and newline['abn'] is None:
                raise KeyError("Line {:2s}{:1d} {:8.3f} is missing the field abn".
                                format(el,newline['ion'],newline['cwave']))

            # add it to the full list
            newlines[el].append(newline)
    return newlines

# verify and prepare the line list
with open('example.lines.yaml') as fin:
       mylines = verify_lines(yaml.load(fin.read()))

#program parameters
outdir = '.'

#object parameters
rv = 15.5 #shift the observations to laboratory scale
vsini = 11.5
vmacro = 0.0
Resol = 110000 #instrumental resolution

#read observations and correct the wavelength scale
obs = Table.read('example.spectrum.obs', format='ascii.no_header')
obs['col1'] = radvelcorr(obs['col1'], rv)

#initiate the fitter with Zeeman line data for the blue region
zdata = pyvald.read_zfile('example.3780-6920.z')
mfit = pysynth.MagSynth(zdata, vsini=vsini, Resol=Resol)

#open the pdf file for saving the line plots
try:
    import pylineid
    fig = plt.figure()
    ax = fig.add_subplot(111)
    pdf = PdfPages(os.path.join(outdir, 'result.example.lines.pdf'))
except ImportError:
    fig = None

#open file for the results
savefile = os.path.join(outdir, 'result.fit.example.lines.yaml')

# initial values for the fitted chemical elements, the values are taken straight
# from the abundance table used to calculate the model atmosphere (abund.tbl)
abund = dict(Ca=-5.75, Ce=-6.71, Co=-5.70, Cr=-4.63, Fe=-3.27, Mg=-5.30,
             Mo=-8.10, Na=-5.50, Ni=-5.41, Sc=-8.30, Si=-4.70, Er=-8.17,
             Gd=-7.28, Ho=-8.30, La=-7.90, Lu=-8.38, Nd=-7.41, Pr=-7.51,
             Sm=-7.68, Tb=-8.40,  Y=-8.65, Dy=-7.50, Sr=-7.25, Ti=-6.45,
             V=-6.83, Zn=-7.44, Zr=-7.93, Ba=-9.87, Tm=-12.04, Yb=-10.96,
             Mn=-5.38, Eu=-7.18, O=-3.66)

#solver options
method = 'leastsq' #Levenberg-Marquiardt algorithm
#method = 'simplex' #Nelder simplex method
#method = 'tnc' #Truncated Newton algorithm with boundaries for the parameters

#general options for the solvers that work with the data here
options = {'leastsq':dict(full_output=1, verbose=True, xtol=1e-4, epsfcn=5e-5, ftol=1e-4),
           'simplex':dict(xtol=0.01, ftol=0.01, full_output=1, disp=5),
           'tnc':dict(approx_grad=True, epsilon=1e-02, accuracy=1e-03, disp=5)}

results = []
for el in mylines.keys():
    for myline in mylines[el]:
        # skip lines that weren't selected for the abundance determination
        if not myline['select']:
            continue

        #get line parameters
        ion = myline['ion']
        cwave = myline['cwave']
        renorm = myline['renorm']
        vcorr = myline['vcorr']

        # set wavelength ranges
        try:
            wstart, wend = myline['wrange']
        except:
            wstart, wend = (cwave - 0.5, cwave + 0.5)
            myline['wrange'] = [wstart, wend]

        #apply velocity correction
        if vcorr is not None:
            wave = obs['col1'] * (1.0 - vcorr / 2.9979246e5) #minus sign!
        else:
            wave = np.asarray(obs['col1'])

        #cut out the required part of the observations
        idx, = np.where((wave >= wstart) & (wave <= wend))
        wave = wave[idx]
        spec = np.copy(obs['col2'][idx])

        #apply normalization corrections
        if renorm is not None:
            spec *= renorm

        # restore the original abundance table
        atable = abund.copy()

        # fit the line
        if myline['fit']:
            #initial guess
            x0 = [atable[el]]

            #initiate the solver
            mfit.setup_fit(wave, spec, [el], atable=atable, B=myline['mfield'])

            #start the fitting procedure
            res = mfit.fit(x0, method, **options[method])

            #save the result back to the original dictionary
            myline['abn'] = round(float(res[0][0]),2)
        else:
            # for some lines we already have an abundance value
            # we copy the value and just recompute the spectrum for the given wavelength region
            atable[pysynth.get_zelem(el)] = float(myline['abn'])

            #copy the observations to the mfit object
            mfit.obs['wl'] = wave
            mfit.obs['sp'] = spec

            #calculate the synthetic profiles
            #Note that the results will be incorrect for the lines that are too close
            #to the Hydrogen lines.
            mfit(wstart=wstart-0.5, wend=wend+0.5, B=np.asarray(myline['mfield']),
                 atable=atable, vsini=vsini, vmacro=vmacro, Resol=Resol)

        #print line info
        print('{:2s}{:1d} {:9.4f}: eps={:6.2f}'.format(el,ion,cwave,myline['abn']))

        #now create and save the plots
        if fig is not None:
            plot_line(mfit, myline, el, fig)
            pdf.savefig(fig)

if fig is not None:
    pdf.close()

with open(savefile, 'w') as fout:
    yaml.dump(mylines, fout)
