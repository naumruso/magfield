# Description of the vstrat example

The goal of this example is to calculate a parametrized and 
(optionally) regularized abundance stratification solution from 
a set of observations of a magnetic star that have been taken at 
multiple points in time (phases).

The code is a thin wrapper around the magnetic stellar synthesis
code Synmast by Oleg Kochukhov (oleg.kochukhov@physics.uu.se),
which is needed for the calculations to be performed. 

Mathematically, the code tries to solve the minimization problem:

`Psi = Chisq(p) + Lambda * R(p)`,                          Eq.(1)

where

`Chisq(p)` is the chi-squared function of the observations and the
synthetic solution with respect to the problem parameters, i.e., 

`Chisq(p) = \sum ((OBS - SYN(p)) * WEIGHTS)^2`,            Eq.(2)

where the summation is done for each wavelength point and phase, 
and `p` is the vector with the fitted parameters, `OBS` is the
vector with all of the observation points, and `SYN` is the 
vector with the synthetic data.

The second part of the right-hand side of Eq.(1) is the 
regularization functional, `R(p)`, that tries to minimize the variation 
of the parameters with respect to how they change in time (phase)
while the parameter `Lambda` sets the amount the regularization 
functional contributes into the overall solution (`Psi`).

In the code, the functional has the form:

`R(p) = \sum_i \sum_j (p_{i,j+1} - p_{i,j})^2`,            Eq.(3)

where `i` represents each type of parameter, and `j` wraps around
the phases.

A solution of the general abundance stratification problem for a 
single phase (point in time) can be defined with four parameters:

  - `eps_up`: abundance at the top layers of the atmosphere,
  - `eps_lo`: abundance at the bottom layers of the atmosphere
  - `d0`: position of the center of the transiton window
  - `Delta`: width of the transiton window.

In this case the abundance, `eps`, with respect to the variable 
`lgrhox`, which is the log10 of `rhox`, the vertical density,
has the following vertical stratification profile:

```
      eps ^
          |                           eps_lo
   eps_lo x                      /------------
          |                    /  |        
          |                  /    |          
          |                /      |
          |              / ^      |          
          |  eps_up    /   |      |          
   eps_up x----------/     |      |          
          |          |     |      |          
          |          |<---Delta-->|         
          |          |     |      |    
          -----------x-----X------x--------> lgrhox
                           d0          
```
which can be expressed mathematically as

```
                 /
                | eps_up, where lgrhox <= d0 - Delta/2
                |
 eps(lgrhox) =  | eps_up + (eps_lo - eps_up) * (lgrhox - d0 + Delta/2)/Delta         Eq.(4)
                |                         for d0 - Delta/2 < lgrhox < do + Delta/2
                | eps_lo, where lgrhox >= d0 + Delta/2.
                 \
```

Following this, the functional has a more concrete form:

```
R(p) = \sum_j (eps_up_{j+1} - eps_up_{j})^2 +
            + (eps_lo_{j+1} - eps_lo_{j})^2 +
            + (d0_{j+1} - d0_{j})^2 +                                                Eq.(5)
            + (Delta_{j+1} - Delta_{j})^2,
```
where the variable `j` goes through the values of the given parameter
for all phase points. 


## `vstrat3d.py`

The task of this script is to open and read the archive , `vstrat.zip`, 
with the necessary data for the example then prepare the input 
data for the fitting procedure, initialize the class, and then 
save the results once the calculations have been completed. 

Most of the functions necessary for the preparation of the input 
data are contained in the file `helpers.py`. It does not have 
any other function. 

It is also worth mentioning that the code can perform parallel 
computations by calling the `Synmast` code multiple times, this 
is controlled through the variable `nproc` in the script. 
However, the code cannot do any distributed computing.

To run the code execute the following command in the prompt:

`python vstrat3d.py`

