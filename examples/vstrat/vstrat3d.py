import os
import zipfile
import cPickle
import shutil

import numpy as np
from astropy.table import Table
from astropy.io import ascii

from magfield import pykrz, vstrat

from helpers import *

# magnetic field configuration for each phase
mfield = [[2522,0,0], [2545,0,0], [2520,0,0], [2457,0,0],
          [2367,0,0], [2350,0,0], [2252,0,0], [2210,0,0],
          [2192,0,0], [2184,0,0], [2198,0,0], [2249,0,0],
          [2285,0,0], [2330,0,0], [2379,0,0], [2465,0,0]]

# needed to fetch the observational data
phases = ['0.009', '0.088', '0.170', '0.248',
          '0.330', '0.346', '0.428', '0.489',
          '0.525', '0.570', '0.651', '0.731',
          '0.772', '0.811', '0.853', '0.928']

# open the archive with the data
zipdata = zipfile.ZipFile('vstrat.zip', 'r')

# wavelength shifts for individual lines (phase dependent)
tbl = ascii.read(zipdata.open('wshifts_Fe2.csv').readlines(), format='csv', comment='#')

# gather the observations (phase dependent)
obsdata = []
for i in range(len(phases)):
    wshift = np.asarray(tbl['wshift'][i*22:(i+1)*22])
    ffx = phases[i].replace('.','p')
    obsdata.append(get_obsdata(zipdata,ffx,wshift,vradial=-21.76,resol=115000.))
weights = compute_weights(obsdata)

# gather the line lists
zdata = get_zdata(zipdata)

# read the atmosphere model from the archive
krz = pykrz.read_krz(zipdata.open('t7250g41_mod5_abnmod1_eqdgrid.krz').readlines())

# we don't need the archive any longer
zipdata.close()

# initial guess for all phases
# strat. fit, 4 params
params0 = [[-5.31,-5.31, -0.5, 0.5], #'0.009'
           [-5.24,-5.24, -0.5, 0.5], #'0.088'
           [-5.20,-5.20, -0.5, 0.5], #'0.170'
           [-5.15,-5.15, -0.5, 0.5], #'0.248'
           [-5.08,-5.08, -0.5, 0.5], #'0.330'
           [-5.09,-5.09, -0.5, 0.5], #'0.346'
           [-5.03,-5.03, -0.5, 0.5], #'0.428'
           [-4.97,-4.97, -0.5, 0.5], #'0.489'
           [-5.00,-5.00, -0.5, 0.5], #'0.525'
           [-4.96,-4.96, -0.5, 0.5], #'0.570'
           [-4.98,-4.98, -0.5, 0.5], #'0.651'
           [-5.04,-5.04, -0.5, 0.5], #'0.731'
           [-5.09,-5.09, -0.5, 0.5], #'0.772'
           [-5.14,-5.14, -0.5, 0.5], #'0.811'
           [-5.20,-5.20, -0.5, 0.5], #'0.853'
           [-5.29,-5.29, -0.5, 0.5]] #'0.928'

# Other parameters (same for everything)
vsini = 5.5
vmacro = 0.0
vradial = -21.76
resol = 115000.

# chemical element to fit
el = 'Fe'

# free parameters (1 is Fix, 0 is Free)
fixparams = [0, 0, 0, 0] #all free, lip and hpp models

# steps to calculate derivatives for each parameter type
dparams = [0.05, 0.05, 0.05, 0.05]

# type of the polynomial used to approximate the vertical stratification gradient
#model = 'homog'
model = 'lip'
#model = 'hpp'

oversample = 1 #don't oversample anything beforehand

# number of processors to use
nproc = 8

# minimal width of the transition zone in number of points
nlayers = 3

# load the data
myvstrat3d = vstrat.VertStrat3D(mfield, obsdata, zdata, krz, vsini=vsini, vmacro=vmacro)

# initiate the parameters common for the fitting procedure
myvstrat3d.setup(el, params0, regpar=1e3, fixparams=fixparams, dparams=dparams, nproc=nproc,
                 model=model, oversample=oversample, nlayers=nlayers, verbose=True)

# basic fit, no regularization
results = myvstrat3d.leastsq()
with open('leastsq.minpack_basic.pyb', 'wb') as fout:
    cPickle.dump(results, fout)

# with regularization, one step
results = myvstrat3d.leastsq_reg()
with open('leastsq_reg.pyb', 'wb') as fout:
         cPickle.dump(results, fout)

# # relax the solution
# for i in range(25):
#     results = myvstrat3d.leastsq_reg()
#     with open('leastsq_reg.{:s}.step_{:d}.pyb'.format(model,i), 'wb') as fout:
#         cPickle.dump(results, fout)
# 
#     # reduce regularization
#     myvstrat3d.regpar /= 2
# 
#     # the new solution becomes the next starting guess
#     xopt = results[0].reshape((myvstrat3d.nphases,myvstrat3d.nfree))
#     myvstrat3d.params0[:,myvstrat3d.ifree] = np.copy(xopt)

# shutdown the workers
myvstrat3d.disconnect()