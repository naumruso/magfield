"""
CONTAINS A FEW HELPER FUNCTIONS THAT COLLECT AND PREPARE THE INPUT DATA.
"""
import numpy as np

from astropy.table import Table
from astropy.io import ascii

from magfield import pyvald, vstrat

def get_zdata(zipdata):
    zlist01 = pyvald.read_zlist(zipdata.open('zlin/Fe1_4404_000.lin').readlines())
    zlist02 = pyvald.read_zlist(zipdata.open('zlin/Fe2_4491_000.lin').readlines())
    zlist03 = pyvald.read_zlist(zipdata.open('zlin/Fe2_4993_000.lin').readlines())
    zlist04 = pyvald.read_zlist(zipdata.open('zlin/Fe2_5018_000.lin').readlines())
    zlist05 = pyvald.read_zlist(zipdata.open('zlin/Fe2_5132_000.lin').readlines())
    zlist06 = pyvald.read_zlist(zipdata.open('zlin/Fe2_5169_000.lin').readlines())
    zlist07 = pyvald.read_zlist(zipdata.open('zlin/Fe_5197-98_000.lin').readlines())
    zlist08 = pyvald.read_zlist(zipdata.open('zlin/Fe1_5217_000.lin').readlines())
    zlist09 = pyvald.read_zlist(zipdata.open('zlin/Fe1_5253_000.lin').readlines())
    zlist10 = pyvald.read_zlist(zipdata.open('zlin/Fe1_5383_000.lin').readlines())
    zlist11 = pyvald.read_zlist(zipdata.open('zlin/Fe1_5397_000.lin').readlines())
    zlist12 = pyvald.read_zlist(zipdata.open('zlin/Fe1_5410_000.lin').readlines())
    zlist13 = pyvald.read_zlist(zipdata.open('zlin/Fe2_5414_000.lin').readlines())
    zlist14 = pyvald.read_zlist(zipdata.open('zlin/Fe_5424-25_000.lin').readlines())
    zlist15 = pyvald.read_zlist(zipdata.open('zlin/Fe1_5434_000.lin').readlines())
    zlist16 = pyvald.read_zlist(zipdata.open('zlin/Fe1_5560_000.lin').readlines())
    zlist17 = pyvald.read_zlist(zipdata.open('zlin/Fe1_5576_000.lin').readlines())
    zlist18 = pyvald.read_zlist(zipdata.open('zlin/Fe1_5775_000.lin').readlines())
    zlist19 = pyvald.read_zlist(zipdata.open('zlin/Fe1_6137_000.lin').readlines())
    zlist20 = pyvald.read_zlist(zipdata.open('zlin/Fe1_6336_000.lin').readlines())
    zlist21 = pyvald.read_zlist(zipdata.open('zlin/Fe2_6432_000.lin').readlines())
    zdata = { 1:zlist01, 2:zlist02, 3:zlist03, 4:zlist04, 5:zlist05,
              6:zlist06, 7:zlist07, 8:zlist08, 9:zlist09,10:zlist10,
             11:zlist11,12:zlist12,13:zlist13,14:zlist14,15:zlist15,
             16:zlist16,17:zlist17,18:zlist18,19:zlist19,20:zlist20,
             21:zlist21}
    return zdata

#only for one phase
def get_obsdata(zipdata, ffx, wshift, vradial=-21.76, resol=110000.):
    obs01  = Table.read(zipdata.open('obs/4400-4410_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs02  = Table.read(zipdata.open('obs/4480-4500_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs03  = Table.read(zipdata.open('obs/4987-5043_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs04  = Table.read(zipdata.open('obs/5112-5170_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs05  = Table.read(zipdata.open('obs/5156-5214_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs06  = Table.read(zipdata.open('obs/5200-5258_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs07  = Table.read(zipdata.open('obs/5336-5396_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs08  = Table.read(zipdata.open('obs/5384-5444_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs09  = Table.read(zipdata.open('obs/5530-5593_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs10 = Table.read(zipdata.open('obs/5739-5803_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs11 = Table.read(zipdata.open('obs/6083-6152_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs12 = Table.read(zipdata.open('obs/6271-6342_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')
    obs13 = Table.read(zipdata.open('obs/6403-6475_{}.obs'.format(ffx)).readlines(), format='ascii.no_header')

    obs = {}
    obs.update({ 1:(vstrat.radvelcorr(obs01['col1'].data, vradial), obs01['col2'].data, obs01['col3'].data)})
    obs.update({ 2:(vstrat.radvelcorr(obs02['col1'].data, vradial), obs02['col2'].data, obs02['col3'].data)})
    obs.update({ 3:(vstrat.radvelcorr(obs03['col1'].data, vradial), obs03['col2'].data, obs03['col3'].data)})
    obs.update({ 4:(vstrat.radvelcorr(obs04['col1'].data, vradial), obs04['col2'].data, obs04['col3'].data)})
    obs.update({ 5:(vstrat.radvelcorr(obs05['col1'].data, vradial), obs05['col2'].data, obs05['col3'].data)})
    obs.update({ 6:(vstrat.radvelcorr(obs06['col1'].data, vradial), obs06['col2'].data, obs06['col3'].data)})
    obs.update({ 7:(vstrat.radvelcorr(obs07['col1'].data, vradial), obs07['col2'].data, obs07['col3'].data)})
    obs.update({ 8:(vstrat.radvelcorr(obs08['col1'].data, vradial), obs08['col2'].data, obs08['col3'].data)})
    obs.update({ 9:(vstrat.radvelcorr(obs09['col1'].data, vradial), obs09['col2'].data, obs09['col3'].data)})
    obs.update({10:(vstrat.radvelcorr(obs10['col1'].data, vradial), obs10['col2'].data, obs10['col3'].data)})
    obs.update({11:(vstrat.radvelcorr(obs11['col1'].data, vradial), obs11['col2'].data, obs11['col3'].data)})
    obs.update({12:(vstrat.radvelcorr(obs12['col1'].data, vradial), obs12['col2'].data, obs12['col3'].data)})
    obs.update({13:(vstrat.radvelcorr(obs13['col1'].data, vradial), obs13['col2'].data, obs13['col3'].data)})

    wreg1  = {'zkey': 1, 'obs':obs[ 1], 'wstart':4404.51, 'wend':4405.01, 'wshift':-wshift[ 0], 'resol':resol, 'weights':1.0}
    wreg2  = {'zkey': 2, 'obs':obs[ 2], 'wstart':4491.30, 'wend':4491.52, 'wshift':-wshift[ 1], 'resol':resol, 'weights':1.0}
    wreg3  = {'zkey': 3, 'obs':obs[ 3], 'wstart':4993.24, 'wend':4993.51, 'wshift':-wshift[ 2], 'resol':resol, 'weights':1.0}
    wreg4  = {'zkey': 4, 'obs':obs[ 3], 'wstart':5018.20, 'wend':5018.62, 'wshift':-wshift[ 3], 'resol':resol, 'weights':1.0}
    wreg5  = {'zkey': 5, 'obs':obs[ 4], 'wstart':5132.52, 'wend':5132.83, 'wshift':-wshift[ 4], 'resol':resol, 'weights':1.0}
    wreg6  = {'zkey': 6, 'obs':obs[ 5], 'wstart':5168.76, 'wend':5169.27, 'wshift':-wshift[ 5], 'resol':resol, 'weights':1.0}
    wreg7  = {'zkey': 7, 'obs':obs[ 5], 'wstart':5198.52, 'wend':5198.88, 'wshift':-wshift[ 6], 'resol':resol, 'weights':1.0}
    wreg8  = {'zkey': 8, 'obs':obs[ 6], 'wstart':5217.17, 'wend':5217.58, 'wshift':-wshift[ 7], 'resol':resol, 'weights':1.0}
    wreg9  = {'zkey': 9, 'obs':obs[ 6], 'wstart':5253.24, 'wend':5253.63, 'wshift':-wshift[ 8], 'resol':resol, 'weights':1.0}
    wreg10 = {'zkey':10, 'obs':obs[ 7], 'wstart':5383.15, 'wend':5383.61, 'wshift':-wshift[ 9], 'resol':resol, 'weights':1.0}
    wreg11 = {'zkey':11, 'obs':obs[ 8], 'wstart':5396.91, 'wend':5397.32, 'wshift':-wshift[10], 'resol':resol, 'weights':1.0}
    wreg12 = {'zkey':12, 'obs':obs[ 8], 'wstart':5410.75, 'wend':5411.12, 'wshift':-wshift[11], 'resol':resol, 'weights':1.0}
    wreg13 = {'zkey':13, 'obs':obs[ 8], 'wstart':5413.89, 'wend':5414.27, 'wshift':-wshift[12], 'resol':resol, 'weights':1.0}
    wreg14 = {'zkey':14, 'obs':obs[ 8], 'wstart':5423.89, 'wend':5424.29, 'wshift':-wshift[13], 'resol':resol, 'weights':1.0}
    wreg15 = {'zkey':14, 'obs':obs[ 8], 'wstart':5425.09, 'wend':5425.43, 'wshift':-wshift[14], 'resol':resol, 'weights':1.0}
    wreg16 = {'zkey':15, 'obs':obs[ 8], 'wstart':5434.38, 'wend':5434.66, 'wshift':-wshift[15], 'resol':resol, 'weights':1.0}
    wreg17 = {'zkey':16, 'obs':obs[ 9], 'wstart':5560.04, 'wend':5560.41, 'wshift':-wshift[16], 'resol':resol, 'weights':1.0}
    wreg18 = {'zkey':17, 'obs':obs[ 9], 'wstart':5575.95, 'wend':5576.26, 'wshift':-wshift[17], 'resol':resol, 'weights':1.0}
    wreg19 = {'zkey':18, 'obs':obs[10], 'wstart':5774.89, 'wend':5775.31, 'wshift':-wshift[18], 'resol':resol, 'weights':1.0}
    wreg20 = {'zkey':19, 'obs':obs[11], 'wstart':6136.44, 'wend':6137.89, 'wshift':-wshift[19], 'resol':resol, 'weights':1.0}
    wreg21 = {'zkey':20, 'obs':obs[12], 'wstart':6336.54, 'wend':6337.12, 'wshift':-wshift[20], 'resol':resol, 'weights':1.0}
    wreg22 = {'zkey':21, 'obs':obs[13], 'wstart':6432.44, 'wend':6432.96, 'wshift':-wshift[21], 'resol':resol, 'weights':1.0}

    wregions = [ wreg1,  wreg2,  wreg3,  wreg4,  wreg5,  wreg6,  wreg7,  wreg8,  wreg9, wreg10,
                wreg11, wreg12, wreg13, wreg14, wreg15, wreg16, wreg17, wreg18, wreg19, wreg20,
                wreg21, wreg22]

    # calculate the SNR for each region
    # this adds another field called snr
    for wregion in wregions:
        w0 = wregion['wstart']*(1-5.5/2.9979246e5)
        w1 = wregion['wend']*(1+5.5/2.9979246e5)
        idx, = np.where((wregion['obs'][0] >= w0) & (wregion['obs'][0] <= w1))
        snr = 1./wregion['obs'][2][idx]
        wregion['snr'] = np.mean(snr)
    return wregions

def compute_weights(obsdata):
    """
    Calculate phase specific weights. Within one phase we fit all profiles with equal weights,
    but between phases we want to have better fit for observations with systematically higher SNR.
    """
    snr = []
    for wregions in obsdata:
        snr_ = 0.0
        for wregion in wregions:
            snr_ += wregion['snr']
        snr.append(snr_ / len(wregions))
    snr = np.asarray(snr, dtype=np.float64)
    weights = snr**2 / np.sum(snr**2)
    weights *= 1000 # this is to avoid numbers too close to zero

    #update the observations with the proper weights
    for i in range(len(obsdata)):
        for wregion in obsdata[i]:
            wregion['weights'] = np.sqrt(weights[i])
    return weights
