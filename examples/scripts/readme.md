# Common usage of `zconcat.py`, `zextract.py`, and `zupdate.py`

These three scripts simplify a few tasks that commonly arise
with long form VALD stellar extract data during manual abundance
analysis of intensity spectra of magnetic stars. This readme
file presentes their common usage and gives a number of 
simple and more complex examples.


## `zupdate.py`

This script is mostly used to update the header of a long form
VALD stellar extract (thereafter a "line list") as well as to
extract a smaller wavelength region from the line list that was
supplied during input. 


For example, the command below
`zupdate.py 3780-6920.z -w 5050 5055 -o 5050-5055.z`
creates a new line list in the wavelength region 5050-5055A. 


It is also possible to modify the header of the input file, e.g., 
  - update the magnetic field vector

    `zupdate.py 5050-5055.z -b 500 5000 5000 -o 5050-5055.newB.z`
  - update the name of the model atmosphere file

    `zupdate.py 5050-5055.z -k atmmodel.krz -o 5050-5055.atmmodel.z`


 Finally, the user can also modify the abundance table specified
 in the input data, e.g., the command

 `zupdate 3780-6920.z -e Fe Ni Cr -a -5.6 -5.5 -6.4 -o 3780-6920.abund_FeNiCr.z`

 sets the abundance values for Fe, Ni, and Cr to -5.6, -5.5, and
 -6.4, and saves the new line list in `3780-6920.abund_FeNiCr.z`.


 ## `zextract.py`

 This script is used to output a new line list from the input 
 data that satisfies certain criteria. The most common usage is 
 to select spectral lines only from a given wavelength range 

 `zextract.py 3780-6920.z -w 6500 6600 -o 6600-6600.z`,

 or to update the atmosphere model or the magnetic field, i.e.,

 `zextract.py 3780-6920.z -k newatm.krz -b 2500 2500 2500 -o 3780-6920.newatm.newB.z`.


 We can additionally select spectral lines that belong to a given
 chemical element. In the example below we select all iron lines 
 that are in the wavelength region from 6500 to 6600 Angstrom,

`zextract.py 3780-6920.z -w 6500 6600 -e Fe -o 6500-6600.Fe.z`.


The code also makes it possible to select spectral lines based on
their ionization stage, e.g., select all FeII lines in the 
6500-6600A range,

`zextract.py 3780-6920.z -w 6500 6600 -e Fe -i 2 -o 6500-6600.FeII.z`.


Finally, it is also possible to select lines with a given residual
central depth by passing the `-c` argument to the code, as well as
to change the name of the file with the atmosphere model, i.e., 

`zextract.py 3780-6920.z -w 6500 6600 -e Fe -i 2 -c 0.1 -k newmodel.krz`

## `zconcat.py`

The script merges two different line lists into a single file. The
resulting file **inherits** the header of the first input line 
list, however the merged line lists are not re-sorted according to
increasing wavelength. They are added one on top of each other.

We can highlight the functionality of the script with the example:

1. Extract all FeI lines in the region 4000-5000A
   `zextract.py 3780-6920.z -w 4000 5000 -e Fe -i 1 -o 4000-5000.FeI.z`

2. Extract all the hydrogen lines
   `zextract.py 3780-6920.z -e H He -o 3780-6920.H.z`

3. Now we can merge the line lists
   `zconcat.py 4000-5000.FeI.z 3780-6920.H.z -o tmp.z`

4. And finally remove the hydrogen lines too far from the 4000-5000 region
   `zupdate.py tmp.z -w 3900 5100 -o 4000-5000.FeI_Balmer.z`









